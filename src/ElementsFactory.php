<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\base\BaseObject;

/**
 * @property array @aliases
 */
class ElementsFactory extends BaseObject
{
    /**
     * @var array `key` - алиас по которому можно создать экземпляр класса, `value` - имя класса элемента
     */
    private static $aliasesMap = [
        // Поля
        'number' => \sdangiriev\yii2\forms\types\NumberType::class,
        'string' => \sdangiriev\yii2\forms\types\StringType::class,
        'text' => \sdangiriev\yii2\forms\types\TextType::class,
        'dropdown' => \sdangiriev\yii2\forms\types\DropDownListType::class,
        'checkbox' => \sdangiriev\yii2\forms\types\CheckboxType::class,
        'radio' => \sdangiriev\yii2\forms\types\RadioType::class,
        'file' => \sdangiriev\yii2\forms\types\FileType::class,
        'date' => \sdangiriev\yii2\forms\types\DateType::class,
        'time' => \sdangiriev\yii2\forms\types\TimeType::class,
        'hidden' => \sdangiriev\yii2\forms\types\HiddenType::class,
        'widget' => \sdangiriev\yii2\forms\types\WidgetType::class,
        'relation' => \sdangiriev\yii2\forms\types\RelationType::class,
        'checkboxList' => \sdangiriev\yii2\forms\types\CheckboxList::class,
        'radioList' => \sdangiriev\yii2\forms\types\RadioList::class,

        // Кнопки
        'button' => \sdangiriev\yii2\forms\buttons\InputButton::class,
        'submitButton' => \sdangiriev\yii2\forms\buttons\SubmitButton::class,
        'resetButton' => \sdangiriev\yii2\forms\buttons\ResetButton::class,

        // Формы
        'form' => \sdangiriev\yii2\forms\Form::class,
        'tabsForm' => \sdangiriev\yii2\forms\TabsForm::class,

        // Особые
        'raw' => \sdangiriev\yii2\forms\RawElement::class,
        'composite' => \sdangiriev\yii2\forms\CompositeElement::class,
        'collection' => \sdangiriev\yii2\forms\CollectionElement::class,
    ];

    /**
     * Регистрация нового алиаса для типа элемента. Этот алиас потом можно использовать при создании элементов формы.
     * Алиасы регистрируются глобально. Т.е. однажды зарегистрированный алиас будет доступен для всех экземпляров
     * класса `ElementsFactory`.
     * 
     * @param string $alias Имя алиаса
     * @param string|array $definition
     *  - `string` - Имя класса
     *  - `array` - Настойки по умолчанию для элемента.
     *              Массив должен содержать элемент 'type' который содержит имя класса или имя ранее зарегистрированного алиаса
     *
     * !Внимание! Регистрируемый класс должен реализовывать интерфейс `FormElementInterface`
     * 
     * @throws InvalidArgumentException Если в качестве значения алиаса передан массив, в котором отсутствует элемент 'type'
     */
    public function registerAlias(string $alias, $definition)
    {
        if (is_array($definition) && !isset($definition['type'])) {
            throw new InvalidArgumentException('Element configuration must contain "type" element.');
        }

        $type = is_array($definition) ? $definition['type'] : $definition;

        if (isset(self::$aliasesMap) || $this->validateClassName($type)) {
            self::$aliasesMap[$alias] = $definition;
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return self::$aliasesMap;
    }

    /**
     * Массовая регистрация алиасов
     * @param array $aliases
     */
    public function setAliases(array $aliases)
    {
        foreach ($aliases as $alias => $config) {
            $this->registerAlias($alias, $config);
        }
    }

    /**
     * Создает элемент формы.
     * 
     * Примеры:
     * ```
     * // Стандартное создание элемента при помощи конфигурационного массива
     * $factory->create('title', [
     *     'type' => 'string',
     *     'label' => 'Title',
     *     'hint' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
     * ]);
     *
     * // Создание элемента с типом 'raw'
     * // Данный тип поддерживает особый способ создания. Это сделано для удобства использования.
     * $factory->create('hrTag', '<hr />');
     * ```
     * 
     * @param string $name
     * @param array|string $config
     * @return FormElementInterface
     */
    public function create($name, $config = []): FormElementInterface
    {
        if (is_string($config)) {
            $config = [
                'type' => 'raw',
                'body' => $config,
            ];
        }

        return $this->createInstance($name, $config);
    }

    /**
     * Создает элемент по конфигурации.
     * Выбрасывает Exception при неправильной конфигурации элемента.
     * Для создания элемента из аргумента `$config` извлекается значение свойства `type`.
     * Значение свойства `type` может быть следующих форматов:
     *  - Алиас, зарегистрированный с помощью функции 'registerAlias()'
     *  - Имя класса. Класс должен реализовывать интерфейс `FormElementInterface`
     *  
     * @param string $name
     * @param array $config
     * @return FormElementInterface
     */
    public function createInstance($name, $config): FormElementInterface
    {
        $type = ArrayHelper::remove($config, 'type');

        if (!$type) {
            throw new InvalidConfigException(strtr('For element with name "{name}" type is not set.', ['{name}' => $name]));
        }

        if (isset(self::$aliasesMap[$type])) {
            if (is_array(self::$aliasesMap[$type])) {
                $definition = self::$aliasesMap[$type];
                $config = ArrayHelper::merge($definition, $config);
                return $this->createInstance($name, $config);
            } else {
                $type = self::$aliasesMap[$type];
            }
        }

        $this->validateClassName($type);

        return Yii::$container->get($type, [$name], $config);
    }

    /**
     * Валидирует имя класса.
     * Класс должен реализовывать интерфейс `FormElementInterface`.
     * 
     * @param string $class
     * @return bool
     * @throws InvalidArgumentException Если класс не существует
     * @throws InvalidArgumentException Если класс не реализует интерфейс `FormElementInterface`
     */
    private function validateClassName($class): bool
    {
        if (!class_exists($class)) {
            throw new InvalidArgumentException(strtr('The class `{class}` does not exists.', ['{class}' => $class]));
        } elseif (!is_subclass_of($class, FormElementInterface::class)) {
            throw new InvalidArgumentException(strtr('The class `{class}` must implement `{interface}`.', [
                '{class}' => $class,
                '{interface}' => FormElementInterface::class,
            ]));
        }
        return true;
    }
}
