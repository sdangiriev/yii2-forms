<?php

namespace sdangiriev\yii2\forms;

/**
 * Интерфейс шаблона формы
 * Главное назначение шаблона - инкапсуляция процесса настройки формы, т.е. в шаблоне можно добавлять поля и установить метод отправки данных
 */
interface FormTemplateInterface
{
	/**
	 * Настраивает форму
	 * @param Form $form
	 */
	public function buildForm(CompositeElementInterface $form);
}

?>