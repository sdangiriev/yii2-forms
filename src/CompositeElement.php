<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Model;
use sdangiriev\yii2\forms\themes\ThemeInterface;

class CompositeElement extends AbstractCompositeElement
{
    /**
     * Конфигурация для реализации `ThemeInterface`.
     * @var array
     */
    public $theme;

    /**
     * TODO Зачем нужно это свойство?
     * 
     * Определяет, нужно ли валидировать данные текущего элемента
     * @var callable|bool
     *  - `bool` - если `false`, валидация элемента запущена не будет
     *  - `callable` - результат, возвращенный функцией, определяет, будет ли запущена валидация
     */
    public $runValidation = true;

    /**
     * Правила валилидации значений элементов
     * @var array
     * @see Model::rules()
     */
    private $_rules = [];

    /**
     * {@inheritdoc}
     */
    public function render(): string
    {
        if (!$this->isVisible()) {
            return '';
        }

        $this->applyTheme();

        return parent::render();
    }

    /**
     * Возвращает список правил валидации
     * @return array
     */
    public function rules(): array
    {
        return $this->_rules;
    }

    /**
     * Добавляем правила валидации полей. Алиас для метода `addRules()`.
     * Правила указываются так же, как и в классах моделей Yii (@see \yii\base\Model::rules()):
     * ```
     * $form->addRules([
     *     [['title', 'description'], 'required'],
     *     [['price'], 'number', 'min' => 100],
     *     ...
     * ])
     * ```
     *
     * !Внимание! Правила валидации не работают для элементов, реализующих интерфейс `CompositeElementInterface`.
     * Т.е. правила можно добавлять только для простых элементов, т.е. обычных полей форм.
     *
     * !Внимание! Фильтры, которые изменяют значения, такие как `DefaultValidator` работать не будут.
     * 
     * @param array $rules
     * @return self
     */
    public function setRules(array $rules)
    {
        $this->addRules($rules);
        return $this;
    }

    /**
     * Добавляет новое правило валидации
     * @param array $rule
     * @return self
     */
    public function addRule(array $rule)
    {
        $this->_rules[] = $rule;
        return $this;
    }

    /**
     * Добавляем массив правил валидации
     * @param array $rules
     * @return self
     */
    public function addRules(array $rules)
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }
        return $this;
    }

    /**
     * Валидация данных.
     *
     * Пример конфигурации элемента с правилами валидации:
     * ```
     * $config = [
     *     'type' => 'composite',
     *     'rules' => [
     *         [['name', 'age'], 'required'],
     *         [['name'], 'string', 'max' => 255],
     *         [['name'], 'match', 'pattern' => '/^...$/'],
     *         [['age'], 'integer', 'max' => 10000],
     *         [['age'], compare', 'compareOperator' => '>', 'compareValue' => 0],
     *     ],
     *     'elements' => [
     *         'name' => [
     *             'type' => 'string',
     *         ],
     *         'age' => [
     *             'type' => 'number',
     *         ],
     *     ],
     * ];
     * ```
     *
     * @param array $fieldNames Список имен полей в которые будут валидироваться
     * @return bool
     */
    public function validate($fieldNames = null): bool
    {
        $valid = parent::validate($fieldNames);
        $labels = [];
        $data = [];

        if ($rules = $this->rules()) {
            foreach ($this->getFields() as $field) {
                if ($field instanceof CompositeElementInterface) {
                    continue;
                }

                $labels[$field->getName()] = $field->getLabel();
                $data[$field->getName()] = $field->getData();
            }

            if ($data) {
                $model = Yii::$container->get(DynamicModel::class, [$data], [
                    'attributeLabels' => $labels,
                    'rules' => $rules,
                ]);

                if (!$model->validate($fieldNames)) {
                    $valid = false;
                    $this->addErrors($model->getErrors());
                }
            }
        }

        return $valid;
    }

    /**
     * Применяет шаблон внешенего вида формы
     * @see static::$theme
     */
    public function applyTheme()
    {
        if ($this->theme) {
            $this->getTheme()->customize($this);
        }
    }

    /**
     * @return ThemeInterface
     */
    protected function getTheme()
    {
        $theme = null;
        if ($this->theme) {
            $theme = Yii::createObject($this->theme);
        } elseif ($this->hasParent()) {
            $theme = $this->getParent()->getTheme();
        }
        return $theme;
    }
}
