<?php

namespace sdangiriev\yii2\forms;

use yii\base\DynamicModel as BaseDynamicModel;

/**
 * Класс `DynamicModel` является расширением класса `yii\base\DynamicModel` и добавляет следущий функционал:
 *  - Массовое добавление правил валидации (@see DynamicModel::addRules())
 *  - Установка правил валидации через конструктор, свойство 'rules' (@see DynamicModel::setRules())
 *  - Установка заголовкой для полей через сеттер (@see DynamicModel::setAttributeLabels())
 *      Это необходимо для правильного отображения заголовкой полей при валидации.
 *      Т.к. класс `yii\base\DynamicModel` не позволяет задавать заголовки для валидируемых полей, в текстах ошибок
 *      мы будем видеть не текущий заголовок поля, а тот, который получился в следствие вызова метод `Model::generateAttributeLabel()`
 * 
 * @property-write array $attributeLabels
 * @property-write array $rules
 */
class DynamicModel extends BaseDynamicModel
{
    /**
     * @var array
     */
    private $_attributeLabels = [];

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return $this->_attributeLabels;
    }

    /**
     * Устанавливает заголовки для атрибутов модели.
     * @param array $attributeLabels
     * @see attributeLabels()
     */
    public function setAttributeLabels(array $attributeLabels): void
    {
        $this->_attributeLabels = $attributeLabels;
    }

    /**
     * Позволяет массово добавлять правила валидации.
     * @param array $rules
     */
    public function addRules(array $rules)
    {
        foreach ($rules as $rule) {
            $this->addRule(array_shift($rule), array_shift($rule), $rule);
        }
    }

    /**
     * Алиас для `addRules()`
     * @param array $rules
     * @see addRules()
     */
    public function setRules(array $rules)
    {
        $this->addRules($rules);
    }
}