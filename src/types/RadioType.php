<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class RadioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        return Html::radio($this->getHtmlName(), $this->getData(), $this->options);
    }
}