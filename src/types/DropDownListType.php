<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class DropDownListType extends AbstractListType
{
    /**
     * Определяет, может ли пользователь выбрать несколько значений
     * @var bool
     */
    public $multiple = false;

    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        $options = $this->options;
        $options['multiple'] = (bool) $this->multiple;
        return Html::dropDownList($this->htmlName, $this->data, $this->items, $options);
    }
}
