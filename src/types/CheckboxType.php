<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class CheckboxType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $_template = "{field}\n{label}\n{description}\n{error}";

    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        return Html::checkbox($this->getHtmlName(), $this->getData(), $this->options);
    }
}
