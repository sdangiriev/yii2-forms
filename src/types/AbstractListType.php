<?php

namespace sdangiriev\yii2\forms\types;

/**
 * @property array $items
 */
abstract class AbstractListType extends AbstractType
{
	/**
	 * Элементы списка
	 * @var array key - значение элемента, value - заголовок, который будет выводиться пользователю в списке
	 */
	protected $_items = [];

	/**
	 * Возвращает элементы списка
	 * @return array
	 */
	public function getItems(): array
	{
		return $this->_items;
	}

	/**
	 * Устанавливает элемента списка
	 * @param array $items
	 */
	public function setItems(array $items)
	{
		$this->_items = $items;
	}
}