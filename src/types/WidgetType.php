<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

class WidgetType extends AbstractType
{
    /**
     * Имя класса виджета
     * @var string
     */
    public $class;

    /**
     * Конфигурация виджета
     * @var array
     */
    public $config = [];

    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        /** @var \yii\base\Widget $class Имя класса */
        $class = $this->class;
        $config = $this->config;
        if (is_subclass_of($class, InputWidget::class)) {
            // InputWidget предназначен для использования с yii\widgets\ActiveForm & yii\widgets\ActiveField классами.
            // Что бы избежать неправильного поведения, виджету не передаются поля 'model' & 'attribute'. Вместо них
            // используются 'name' & 'value'.
            $config['name'] = $this->htmlName;
            $config['value'] = $this->data;
            $config['options'] = ArrayHelper::merge($this->options, $config['options'] ?? []);
        }
        return $class::widget($config);
    }
}
