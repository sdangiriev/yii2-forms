<?php

namespace sdangiriev\yii2\forms\types;

use yii\base\Model;
use sdangiriev\yii2\forms\FieldElementInterface;

/**
 * Класс `AbstractType` реализует функционал для работы с моделями.
 * Например, если поле прикреплено к модели (@see AbstractType::$mapped), то label может браться из привязанной модели.
 * Текст описания поля (@see AbstractType::getDescription()), ошибки валидации и другие данные также могут быть получены из модели.
 */
abstract class AbstractType extends AbstractHtmlField
{
    /**
     * Свойство определяет, будет ли данный элемент формы связан с моделью.
     * Если значение равно `true` - элемент будет привязан к свойству модели.
     * Если значение равно `false` - модель не может быть запиана в свойство `static::$model`, а также не может использоваться с этим элементом.
     * Модель может быть добавлена элементу только если значение этого свойство установлено в `true`.
     * @var bool
     */
    public $mapped = true;

    /**
     * @var mixed
     */
    protected $_data;

	/**
	 * Массив сообщений об ошибках
	 * @var string[]
	 */
	protected $_errors = [];

    /**
     * {@inheritdoc}
     */
	public function init()
    {
        parent::init();
        /**
         * Добавляет правила валидации значения поля.
         * Правила валидации будут добавлены только в случае, если установлен родительский элемент
         * и поле не привязано к модели. Если поле привязано к какой-либо модели, правила валидации будут взяты
         * из привязанной модели.
         */
        if ($this->hasParent() && !$this->isMapped()) {
            $rules = [];
            foreach ($this->defaultRules() as $key => $rule) {
                array_unshift($rule, [$this->getName()]);
                $rules[$key] = $rule;
            }
            if ($rules) {
                $this->getParent()->addRules($rules);
            }
        }
    }

    /**
     * Возвращает массив правил для валидации поля.
     * Каждое поле может для себя определять каким образом его валидировать.
     * Данные правила будут применяться только в том случае, если поле не имеет модели, т.е. `self::isMapped() === false`.
     * @return array
     */
    protected function defaultRules(): array
    {
        return $this->required ? [['required']] : [];
    }

    /**
	 * {@inheritdoc}
	 */
	public function getLabel(): string
	{
		return parent::getLabel() ?: ($this->isMapped() ? $this->model()->getAttributeLabel($this->getName()) : '');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getDescription(): string
	{
		return parent::getDescription() ?: ($this->isMapped() ? $this->model()->getAttributeHint($this->getName()) : '');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getData()
	{
		return $this->isMapped() ? $this->model()->{$this->getName()} : $this->_data;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setData($data)
	{
		if ($this->isMapped()) {
			$this->model()->{$this->getName()} = $data;
		} else {
			$this->_data = $data;
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function load($data, $scopeName = ''): bool
	{
		$loaded = false;

		if ($scopeName === '') {
			$this->setData($data);
			$loaded = true;
		} elseif (is_array($data)) {
			$scopeName = $scopeName ?? $this->getName();
			if (array_key_exists($scopeName, $data)) {
				$this->setData($data[$scopeName]);
				$loaded = true;
			}
		}

		return $loaded;
	}

    /**
     * {@inheritdoc}
     */
    public function getErrors(): array
    {
        return $this->isMapped()
            ? array_merge($this->_errors, $this->model()->getErrors($this->getName()))
            : $this->_errors;
    }

    /**
     * {@inheritdoc}
     */
    public function clearErrors()
    {
        parent::clearErrors();
        if ($this->isMapped()) {
            $this->model()->clearErrors($this->getName());
        }
    }

    /**
     * Определяет, привязанно ли данное поле к атрибуту модели.
     * Вернет `true` только в случае когда `static::$mapped === true` и родительский элемент имеет модель.
     * @return bool
     */
    public function isMapped(): bool
    {
        return $this->mapped && $this->hasParent() && $this->getParent()->hasModel();
    }

    /**
     * Возвращает модель, к одному из атрибутов которой привязан данный элемент формы
     * @return Model|null
     */
    protected function model(): ?Model
    {
        return $this->isMapped() ? $this->getParent()->getModel() : null;
    }
}
