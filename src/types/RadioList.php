<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class RadioList extends AbstractListType
{
    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        $output = '';

        foreach ($this->getItems() as $value => $label) {
            $options = $this->options;
            $options['id'] = preg_replace('/\./', '-', $this->resolveName() . '.' . $value);
            $options['value'] = $value;
            $output .= Html::radio($this->getHtmlName(), $value === $this->getData(), $options) . ' ' . Html::label($label, $options['id']);
        }

        return $output;
    }
}
