<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class DateType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function renderField(): string
	{
		return Html::input('date', $this->htmlName, $this->data, $this->options);
	}
}

?>