<?php

namespace sdangiriev\yii2\forms\types;

use yii\base\Model;
use yii\helpers\Html;
use sdangiriev\yii2\forms\AbstractFormField;
use sdangiriev\yii2\forms\WrapperOwnerTrait;

/**
 * Содержит методы и свойства которые позволяют работать с полем, как с HTML-элементом: настривать атрибуты, заголовки, описание и т.д.
 * 
 * @property string $error
 * @property-read string $htmlId
 * @property-read string $htmlName
 */
abstract class AbstractHtmlField extends AbstractFormField
{
	use WrapperOwnerTrait;

	/**
	 * HTML-атрибуты поля
	 * @var array
	 */
	public $options = [];

	/**
	 * HTML-атрибуты заголовка поля (`<label>`)
	 * @var array
	 */
	public $labelOptions = [];

	/**
	 * HTML-атрибуты элемента подсказки
	 * @var array
	 */
	public $descriptionOptions = [];

	/**
	 * HTML-атрибуты элемента ошибки
	 * @var array
	 */
	public $errorOptions = [];

	/**
	 * Css-класс который будет добавлен полю, которое содержит ошибку валидации
	 * @var string
	 */
	public $errorCssClass = '';

	/**
	 * Определяет, является ли поле обязательным для заполнения
	 * @var bool
	 */
	public $required = false;

	/**
	 * CSS-класс который будет добавлен заголовкам обязательных полей
	 * @var string
	 */
	public $requiredCssClass = 'required';

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		$this->options['id'] = $this->htmlId;
	}

	/**
	 * Ренедеринг заголовка поля (`<label>`).
	 * Если заголовок поля пустой, элемент не будет отображен.
	 * @return string
	 */
	protected function renderLabel(): string
	{
		$output = '';

		if ($label = $this->getLabel()) {
			$for = $this->labelOptions['for'] ?? $this->options['id'] ?? $this->getHtmlId();
			$options = $this->labelOptions;

			if ($this->required && $this->requiredCssClass) {
				Html::addCssClass($options, $this->requiredCssClass);
			}

			$output = Html::label($label, $for, $options);
		}

		return $output;
	}

	/**
	 * Ренедеринг описания поля
	 * @return string
	 */
	protected function renderDescription(): string
	{
		return $this->getDescription()
			? Html::tag('div', $this->getDescription(), $this->descriptionOptions)
			: '';
	}

	/**
	 * Ренедеринг ошибки
	 * @return string
	 */
	protected function renderError(): string
	{
		$output = '';

		if ($this->hasErrors()) {
			$this->wrapper['options'] = $this->wrapper['options'] ?? [];
			Html::addCssClass($this->wrapper['options'], $this->errorCssClass);
			$output = Html::tag('div', $this->getFirstError(), $this->errorOptions);
		}

		return $output;
	}

	/**
	 * Рендеринг поля
	 * Данный метод должен быть переопределен в каждом дочернем классе
	 * @return stirng
	 */
	abstract protected function renderField(): string;

	/**
	 * Возвращает шаблон поля для рендеринга
	 * @return string
	 */
	public function getTemplate(): string
	{
		$template = "{label}\n{field}\n{description}\n{error}";

		if ($ownTemplate = parent::getTemplate()) {
			$template = $ownTemplate;
		} elseif ($this->hasParent() && $this->getParent()->getElementsTemplate() !== null) {
			$template = $this->getParent()->getElementsTemplate();
		}

		return $template;
	}

	/**
	 * Возвращает значение html-атрибута 'id'
	 * @return string
	 */
	public function getHtmlId(): string
	{
		return $this->options['id'] ?? 'field-' . str_replace('.', '-', $this->resolveName());
	}

	/**
	 * Возвращает значение для html-атрибута 'name' поля
	 * Зависит от привязанной к полю формы
	 * Если никакая форма не приязана, вернет `static::$id`
	 * Если есть форма, сгенерирует имя использую метод `$form->formName()` для создания имени в контексте формы
	 * @return string
	 */
	public function getHtmlName(): string
	{
		$fullName = $this->resolveName();
		$isFirst = true;
		$name = '';
		foreach (explode('.', $fullName) as $part) {
			if ($isFirst) {
				$name .= $part;
				$isFirst = false;
			} else {
				$name .= "[{$part}]";
			}
		}
		return $name;
	}

	/**
	 * Проверяет наличие сообщений об ошибках
	 * @return bool
	 */
	public function hasErrors(): bool
	{
		return (bool) $this->getErrors();
	}

	/**
	 * Возвращает массив сообщений об ошибках поля
	 * @return array
	 */
	public function getErrors(): array
	{
		return $this->_errors;
	}

    /**
     * Возвращает текст первого особщения об ошибке
     * @return string
     */
	public function getFirstError(): ?string
	{
		$errors = $this->getErrors();
		return $errors ? reset($errors) : '';
	}

    /**
     * Добавляет новое сообщение об ошибке
     * @param string $message Текст ошибки
     */
	public function addError(string $message)
	{
		$this->_errors[] = $message;
	}

    /**
     * Массовое добавление сообщений об ошибках
     * @param string[] $errors
     */
    public function addErrors(array $errors)
    {
        $this->_errors = array_merge($this->_errors, $errors);
    }

    /**
     * Удаляет все имеющиеся сообщения об ошибках
     */
	public function clearErrors()
	{
		$this->_errors = [];
	}
}
