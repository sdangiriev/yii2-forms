<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class StringType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        return Html::textInput($this->htmlName, $this->data, $this->options);
    }
}
