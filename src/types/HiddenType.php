<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class HiddenType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function render(): string
	{
		return $this->renderField();
	}

	/**
	 * {@inheritdoc}
	 */
	protected function renderField(): string
	{
		return Html::hiddenInput($this->htmlName, $this->data, $this->options);
	}
}

?>