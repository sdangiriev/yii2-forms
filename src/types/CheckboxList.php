<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class CheckboxList extends AbstractListType
{
    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        $output = '';

        foreach ($this->getItems() as $value => $label) {
            $options = $this->options;
            $options['id'] = preg_replace('/\./', '-', $this->resolveName() . '.' . $value);
            $options['value'] = $value;
            $output .= Html::checkbox($this->getHtmlName(), in_array($value, $this->getData()), $options) . ' ' . Html::label($label, $options['id']);
        }

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return parent::getData() ?: [];
    }

    /**
     * {@inheritdoc}
     */
    public function getHtmlName(): string
    {
        return parent::getHtmlName() . '[]';
    }
}
