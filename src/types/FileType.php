<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;
use yii\web\UploadedFile;

class FileType extends AbstractType
{
	/**
	 * Определяет, можно ли загружать несколько файлов
	 * @var bool
	 */
	public $multiple = false;

	/**
	 * Список разрешенных типов файлов
	 * @var array|string
	 */
	public $accept;

	/**
	 * Минимальный размер загружаемого файла в байтах
	 * @var int
	 */
	public $minSize;

	/**
	 * Максимальный размер загружаемого файла в байтах
	 * @var int
	 */
	public $maxSize;

	/**
	 * {@inheritdoc}
	 */
	public function defaultRules(): array
	{
		$rules = parent::defaultRules();

		if (($rule = $this->createRule()) !== false) {
			$rules[] = $rule;
		}

		return $rules;
	}

	/**
	 * {@inheritdoc}
	 */
	public function load($data, $scopeName = null): bool
	{
		return $this->loadFiles();
	}

	/**
	 * Загружает отправленные через форму файлы
	 * @return bool
	 */
	private function loadFiles(): bool
	{
		$data = $this->multiple
			? UploadedFile::getInstancesByName($this->getOriginalHtmlName())
			: UploadedFile::getInstanceByName($this->getOriginalHtmlName());
		$this->setData($data);
		return (bool) $data;
	}

	/**
	 * {@inheritdoc}
	 */
	public function renderField(): string
	{
		$options = $this->options;
		if ($this->multiple) {
			$options['multiple'] = true;
		}
		if ($this->accept) {
			$options['accept'] = implode(', ', $this->accept);
		}
		// Добавляем скрытое поле с таким же именем как и у оригинального поля для загрузки файлов.
		// Поле с типом 'file' не добавляется в массив $_POST и для того что бы отследить отправку поля с типом 'file' добавляется скрытое поле
		return Html::hiddenInput($this->getHtmlName()) . Html::fileInput($this->htmlName, $this->getHtmlValue(), $options);
	}

	/**
	 * Возвращает текущее значения поля для отображения в HTML
	 * @return string
	 */
	private function getHtmlValue()
	{
		$currentData = $this->data;
		return is_array($currentData) ? reset($currentData) : $currentData;
	}

	/**
	 * Возвращает оригинальное HTML-имя поля без добавления '[]' в конце
	 * @return string
	 */
	private function getOriginalHtmlName(): string
	{
		return parent::getHtmlName();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getHtmlName(): string
	{
		$name = parent::getHtmlName();
		return $this->multiple ? "{$name}[]" : $name;
	}

	/**
	 * Возвращает правило валидации для текущего поля, которое будет собрано из настроек поля
	 * @return array|null `null` - если нет данных для создания правила валидации
	 * @see $accept
	 * @see $minSize
	 * @see $maxSize
	 */
	public function createRule()
	{
		$rule = null;

		if ($this->accept !== null || $this->minSize !== null || $this->maxSize !== null) {
			$rule = ['file'];

			if ($this->accept !== null) {
				$rule['extensions'] = $this->accept;
			}

			if ($this->minSize !== null) {
				$rule['minSize'] = $this->minSize;
			}

			if ($this->maxSize !== null) {
				$rule['maxSize'] = $this->maxSize;
			}
		}

		return $rule === null ? false : $rule;
	}
}
