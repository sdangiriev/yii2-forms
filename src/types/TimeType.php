<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class TimeType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function renderField(): string
	{
		return Html::input('time', $this->htmlName, $this->data, $this->options);
	}
}

?>