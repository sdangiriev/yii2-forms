<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class TextType extends AbstractType
{
	/**
	 * Количество отображаемых строк (читай "высота")
	 * @var integer
	 */
	public $rows = 4;

	/**
	 * {@inheritdoc}
	 */
	public function renderField(): string
	{
		$defaultOptions = [
			// Отключаем изменение размеров поля по горизонтали по умолчанию
			'style' => ['resize' => 'vertical'],
		];
		$options = array_merge($defaultOptions, $this->options, ['rows' => $this->rows]);
		return Html::textarea($this->htmlName, $this->data, $options);
	}
}

?>