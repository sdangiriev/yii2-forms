<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\ArrayHelper;

class RelationType extends DropDownListType
{
	/**
	 * Имя ActiveRecord класса из которого будут извлекаться записи
	 * @var string
	 */
	public $class = '';

	/**
	 * Имя атрибута, занчение которого будет использовано значение элемента списка
	 * @var string
	 */
	public $valueAttribute = '';

	/**
	 * Имя атрибута, значение которого будет использовано как ярлык, выводимый в интерфейс
	 * @var string
	 */
	public $labelAttribute = '';

	/**
	 * Условие для фильтрации записей. Значение этого поля будет передано в `\yii\db\ActiveQuery::where()`
	 * @var array
	 * @see \yii\db\QueryInterface::where()
	 */
	public $filter = [];

	/**
	 * {@inheritdoc}
	 */
	public function getItems(): array
	{
		$class = $this->class;
		$rows = $class::find()
			->select([$this->valueAttribute, $this->labelAttribute])
			->where($this->filter)
			->all();

		return ArrayHelper::map($rows, $this->valueAttribute, $this->labelAttribute);
	}
}