<?php

namespace sdangiriev\yii2\forms\types;

use yii\helpers\Html;

class NumberType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function renderField(): string
    {
        return Html::input('number', $this->htmlName, $this->data, $this->options);
    }
}
