<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\base\Model;
use yii\base\InvalidArgumentException;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;

/**
 * Список доступных свойств экземпляра класса `Forms` определяется списком добавленных в форму полей (@see Forms::add())
 * При добавлении каждому полю присваивается имя по которому можно будет взаимодействовать с данным полем
 * Например, можно получить текущее значение поля просто обратившсь к свойству формы с таким же именем 
 * как и имя поля, значение котрого хотим получить:
 * ```
 * $form->add('name', StringType::class, [
 * 		'label' => 'Имя',
 * 		'value' => 'John',
 * ]);
 * echo $form->name; // John
 * ```
 *
 * @property string $action
 * @property string $method
 * @property string $error
 * @property string $name
 * @property array $options
 */
class DynamicModelPrototype extends Model
{
	/**
	 * Массив полей формы
	 * @var sdangiriev\yii2\forms\types\BaseType[]
	 */
	private $_fields = [];
	/**
	 * Массив кнопок формы
	 * @var array
	 */
	private $_buttons = [];
	/**
	 * Текст ошибки формы
	 * @var string
	 */
	private $_error;
	/**
	 * Имя формы
	 * Определяет пространство имен для полей формы
	 * Используется в методе `Form::formName()`
	 * Пример
	 * ```
	 * $form = new Form(['name' => 'user'])->add('surname', StringType::class);
	 * 
	 * // Как будет выглядеть поле 'url' после рендеринга
	 * <input type="text" name="user[surname]">
	 * ```
	 *
	 * Поле так же поддерживает объявление сложных имен, например
	 * ```
	 * $form = new Form(['name' => 'user.emails.0'])->add('email', StringType::class);
	 * 
	 * // Как будет выглядеть поле 'url' после рендеринга
	 * <input type="text" name="user[emails][0][email]">
	 * ```
	 * 
	 * Загрузка данных в форму с помощтю метода `static::load()`:
	 * ```
	 * $participantForm = new Form(['name' => 'event.participants.0'])
	 *      ->add('name', StringType::class)
	 *      ->add('email', StringType::class);
	 *
	 * $data = [
	 *     'event' => [
	 *         'participants' => [
	 *             [
	 *                 'name' => 'John',
	 *                 'email' => 'example@mail.com',
	 *             ]
	 *         ]
	 *     ]
	 * ];
	 *
	 * $participantForm->load($data);
	 *
	 * echo $participantForm->name;    // John
	 * echo $participantForm->email;   // example@mail.com
	 * ```
	 * 
	 * @var string
	 * @see Form::formName()
	 */
	public $name;
	/**
	 * URL для отправки формы
	 * @var string
	 */
	protected $action = '';
	/**
	 * Метод отправки фомры - GET || POST
	 * @var string
	 */
	protected $method = 'post';
	/**
	 * HTML-атрибуты формы
	 * @var array
	 */
	public $options = [];

	public function __get($name)
	{
		return $this->hasAttribute($name) ? $this->getAttribute($name) : parent::__get($name);
	}

	public function __set($name, $value)
	{
		if ($this->hasAttribute($name)) {
			$this->setAttribute($name, $value);
		} else {
			parent::__set($name, $value);
		}
	}

	public function __isset($name)
	{
		return $this->hasAttribute($name) || parent::__isset($name);
	}

	public function __unset($name)
	{
		if ($this->hasAttribute($name)) {
			$this->setAttribute($name, null);
		} else {
			parent::__unset($name);
		}
	}

	/**
	 * Добавляет новое поле в форму
	 * @param string $id
	 * @param string $class
	 * @param array  $options
	 * @return Form
	 */
	public function add($id, string $class, array $options = [])
	{
		$this->_fields[$id] = $this->buildField($id, $class, $options);
		// Что это? - Видимо установка значения по умолчанию
		// $this->setAttribute($id, $this->_fields[$id]->value);
		return $this;
	}

	/**
	 * Добавляет новую кнопку в форму
	 * @param strin $class
	 * @param string $label Содержимое кнопки
	 * @param array  $config
	 */
	public function addButton($class, $label, $config = [])
	{
		$this->_buttons[] = Yii::createObject($class, [$label, $config]);
		return $this;
	}

	/**
	 * Устанавливает значение html-атрибута "action" 
	 * @param string $action
	 * @return Form
	 */
	public function setAction(string $action)
	{
		$this->action = $action;
		return $this;
	}

	/**
	 * Устанавливает значение html-атрибута "action"
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * Устанавливает значение html-атрибута "method"
	 * @param string $method
	 * @return Form
	 */
	public function setMethod(string $method)
	{
		$this->method = $method;
		return $this;
	}

	/**
	 * Возвращает зачение html-атрибута "method"
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * Возвращает поле с указанным ID или `null` в случаего его отсутствия
	 * @param  string $id
	 * @return object|null
	 */
	public function field($id)
	{
		return $this->_fields[$id] ?? null;
	}

	/**
	 * Возвращает список всех полей формы
	 * @return \sdangiriev\yii2\forms\types\BaseType[]
	 */
	public function getFields(): array
	{
		return $this->_fields;
	}

	/**
	 * Возвращает список всех кнопок формы
	 * @return \sdangiriev\yii2\forms\buttons\BaseButton[]
	 */
	public function getButtons(): array
	{
		return $this->_buttons;
	}

	/**
	 * Возвращает данные формы
	 * @return array
	 */
	public function getData(): array
	{
		return $this->getAttributes();
	}

	/**
	 * Проверяет наличие атрибута
	 * @param  string $attribute
	 * @return bool
	 */
	private function hasAttribute($attribute)
	{
		return in_array($attribute, $this->attributes());
	}

	/**
	 * Получает значение атрибута
	 * @param  string $attribute
	 * @return mixed
	 */
	private function getAttribute($attribute)
	{
		return $this->hasAttribute($attribute) 
			? $this->field($attribute)->getValue()
			: null;
	}

	/**
	 * Устанавливает атрибут
	 * @param string $attribute
	 * @param mixed $value
	 */
	private function setAttribute($attribute, $value)
	{
		if ($this->hasAttribute($attribute)) {
			$this->field($attribute)->setValue($value);
		} else {
			throw new InvalidArgumentException(get_class($this) . ' has no attribute named "' . $name . '".');
		}
	}

	/**
	 * Переопределяем для того, что бы могли использовать базовый функционал класса `yii\base\Model`,
	 * например, загрузку данных в форму (и поля) с помощью метода `Form::load()`
	 * 
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		return [
			static::SCENARIO_DEFAULT => $this->attributes(),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributes()
	{
		return array_filter(array_keys($this->_fields));
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		$rules = [];
		foreach ($this->_fields as $id => $field) {
			foreach ($field->rules() as $rule) {
				array_unshift($rule, [$id]);
				$rules[] = $rule;
			}
		}
		return $rules;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validate($attributeNames = null, $clearErrors = true)
	{
		return parent::validate($attributeNames, $clearErrors) && !$this->hasError();
	}

	/**
	 * {@inheritdoc}
	 */
	public function load($data, $formName = null)
	{
		if ($formName === null) {
			$formName = '';
			$data = ArrayHelper::getValue($data, $this->name);
		}
		return parent::load($data, $formName);
	}

	/**
	 * {@inheritdoc}
	 */
	public function formName()
	{
		if ($this->name) {
			$isFirst = true;
			$explodedName = array_map(function($item) use (&$isFirst) {
				if ($isFirst) {
					$isFirst = false;
					return strtolower($item);
				}
				return strtolower("[{$item}]");
			}, explode('.', $this->name));
			return implode('', $explodedName);
		}
		return strtolower(parent::formName());
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		$labels = ArrayHelper::map($this->_fields, 'id', 'label');
		return array_filter($labels) ?? [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeHints()
	{
		$hints = ArrayHelper::map($this->_fields, 'id', 'hint');
		return array_filter($hints);
	}

	/**
	 * Проверяет наличие ошибок формы
	 * @return bool
	 */
	public function hasError(): bool
	{
		return $this->_error !== null;
	}

	/**
	 * Возвращает текст текущей ошибки формы
	 * @return string
	 */
	public function getError(): string
	{
		return (string) $this->_error;
	}

	/**
	 * Устанавливает текст текущей ошибки формы
	 * @param string $text
	 */
	public function setError(string $error)
	{
		$this->_error = $error;
	}

	/**
	 * Очищает текст текущей ошибки формы
	 */
	public function clearError()
	{
		$this->_error = null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function hasErrors($attribute = null)
	{
		if ($attribute) {
			return $this->field($attribute)->hasError();
		}
		foreach ($this->_fields as $id => $field) {
			if ($field->hasError()) {
				return true;
			}
		}
		return $this->hasError();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getErrors($attribute = null)
	{
		if ($attribute) {
			return [$this->field($attribute)->getError()];
		}
		$errors = [];
		foreach ($this->_fields as $id => $field) {
			if ($field->hasError()) {
				$errors[$id] = [$field->getError()];
			}
		}
		return $errors;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFirstErrors()
	{
		return $this->getErrors();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFirstError($attribute)
	{
		return $this->field($attribute)->getError() ?: null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function addError($attribute, $error = '')
	{
		$this->field($attribute)->setError($error);
	}

	/**
	 * {@inheritdoc}
	 */
	public function addErrors(array $errors)
	{
		foreach ($errors as $attribute => $error) {
			$this->field($attribute)->setError($error);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function clearErrors($attribute = null)
	{
		if ($attribute) {
			$this->field($attribute)->clearError();
		} else {
			foreach ($this->_fields as $id => $field) {
				$field->clearError();
			}
		}
		$this->clearError();
	}

	/**
	 * Создание объекта поля формы
	 * @param  string $id
	 * @param  string $class
	 * @param  array $options
	 * @return sdangiriev\yii2\forms\types\BaseType
	 */
	private function buildField($id, $class, $options)
	{
		$field = Yii::createObject($class, [$id, $options]);
		$field->setForm($this);
		return $field;
	}
}

?>