<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\base\BaseObject;
use yii\web\View;

/**
 * Базовая реализация интерфейса `FormElementInterface`.
 * Наследуется от класса `BaseObject` что позволяет конфигурировать экземпляры класса
 * при помощи конфигурационного массива (@see $config - аргумент конструктора)
 *
 * @property View $view
 */
abstract class AbstractElement extends BaseObject implements FormElementInterface
{
    /**
     * Определяет видимость элемента
     * @var bool `false` - элемент не будет отображен
     */
    public $visible = true;

    /**
     * Определяет, должен ли элемент настраиваться при помощи тем
     * @var bool
     */
    public $customizable = true;

    /**
     * @var string
     * @see getName(), setName()
     */
    protected $_name;

    /**
     * @var View
     * @see getView(), setView()
     */
    protected $view;

    /**
     * @var CompositeElementInterface|null
     * @see getParent(), setParent()
     */
    protected $_parent;

    /**
     * @param string|int $name Имя элемента. Будет преобразованно к строке
     * @param array $config Конфигурация элемента
     */
    public function __construct($name, $config = [])
    {
        $this->setName((string)$name);
        unset($config['name']); // На всякий случай удаляем ключ 'name' из конфигурации
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName(string $name)
    {
        $this->_name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveName(): string
    {
        return $this->hasParent() ? "{$this->getParent()->resolveName()}.{$this->getName()}" : $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?CompositeElementInterface
    {
        return $this->_parent;
    }

    /**
     * {@inheritdoc}
     * При попытке сброить родителький элемент (установить `null`), текущий элемент будет так же удален из родителя
     */
    public function setParent(?CompositeElementInterface $parent)
    {
        $oldParent = $this->getParent();
        $this->_parent = $parent;
        if ($this->getParent() === null && $oldParent) {
            $oldParent->remove($this->getName());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasParent(): bool
    {
        return (bool)$this->getParent();
    }

    /**
     * {@inheritdoc}
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @return View|null
     */
    public function getView(): ?View
    {
        return $this->view;
    }

    /**
     * @param View $view
     */
    public function setView(View $view)
    {
        $this->view = $view;
    }

    public function __toString()
    {
        try {
            return $this->isVisible() ? $this->render() : '';
        } catch (\Exception $e) {
            if (YII_DEBUG) {
                throw $e;
            } else {
                Yii::error($e->getMessage());
                return '';
            }
        }
    }
}
