<?php

namespace sdangiriev\yii2\forms\buttons;

use yii\helpers\Html;

class InputButton extends AbstractButton
{
	/**
	 * {@inheritdoc}
	 */
	public function render(): string
	{
		return Html::buttonInput($this->label, $this->options);
	}
}