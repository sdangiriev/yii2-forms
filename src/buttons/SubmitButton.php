<?php

namespace sdangiriev\yii2\forms\buttons;

use yii\helpers\Html;

class SubmitButton extends AbstractButton
{
	/**
	 * {@inheritdoc}
	 */
	public function render(): string
	{
		return Html::submitInput($this->label, $this->options);
	}
}