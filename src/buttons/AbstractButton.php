<?php

namespace sdangiriev\yii2\forms\buttons;

use sdangiriev\yii2\forms\AbstractElement;

abstract class AbstractButton extends AbstractElement
{
    /**
     * @var string
     */
    public $label;

    /**
     * @var array
     */
    public $options = [];

    /**
     * Имя стиля кнопки
     * @var string
     */
    public $style;

    public function __construct($name, $config = [])
    {
        parent::__construct($name, is_string($config) ? ['label' => $config] : $config);
    }
}
