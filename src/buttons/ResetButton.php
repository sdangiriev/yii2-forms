<?php

namespace sdangiriev\yii2\forms\buttons;

use yii\helpers\Html;

class ResetButton extends AbstractButton
{
	/**
	 * {@inheritdoc}
	 */
	public function render(): string
	{
		return Html::resetInput($this->label, $this->options);
	}
}