<?php

namespace sdangiriev\yii2\forms\themes;

/**
 * @property-read array $rules
 */
interface ThemeInterface extends CustomizerInterface
{
    /**
     * Возвращает список правил для обработки элементов
     * @return array
     */
    public function getRules(): array;

    /**
     * Добавляет новое правило
     * @param string $elementClass
     * @param string $customizerClass
     */
    public function addRule($elementClass, $customizerClass);
}
