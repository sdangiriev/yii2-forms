<?php

namespace sdangiriev\yii2\forms\themes\bootstrap3;

use sdangiriev\yii2\forms\themes\AbstractCustomizer;
use sdangiriev\yii2\forms\FormElementInterface;

class ButtonCustomizer extends AbstractCustomizer
{
    /**
     * {@inheritdoc}
     */
    public function customizeElement(FormElementInterface $element)
    {
        $this->addCssClass($element->options, 'btn');
    }
}