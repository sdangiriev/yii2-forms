<?php

namespace sdangiriev\yii2\forms\themes\bootstrap3;

use sdangiriev\yii2\forms\themes\AbstractCustomizer;
use sdangiriev\yii2\forms\FormElementInterface;
use sdangiriev\yii2\forms\types\FileType;
use sdangiriev\yii2\forms\types\CheckboxList;
use sdangiriev\yii2\forms\types\CheckboxType;
use sdangiriev\yii2\forms\types\RadioList;
use sdangiriev\yii2\forms\types\RadioType;

class FieldCustomizer extends AbstractCustomizer
{
    /**
     * {@inheritdoc}
     */
    public function customizeElement(FormElementInterface $element)
    {
        if (!$this->canCustomize($element)) {
            return;
        }

        if ($element->wrapper !== false) {
            $element->wrapper = is_array($element->wrapper) ? $element->wrapper : [];
            $element->wrapper['options'] = $element->wrapper['options'] ?? [];
            $this->addCssClass($element->wrapper['options'], 'form-group');
        }

        if (
            !$element instanceof FileType &&
            !$element instanceof CheckboxType &&
            !$element instanceof RadioType &&
            !$element instanceof CheckboxList &&
            !$element instanceof RadioList
        ) {
            $this->addCssClass($element->options, 'form-control');
        }

        $this->addCssClass($element->labelOptions, 'control-label');
        $this->addCssClass($element->descriptionOptions, 'hint-block');
        $this->addCssClass($element->errorOptions, 'help-block');

        $element->errorCssClass = 'has-error';
    }
}