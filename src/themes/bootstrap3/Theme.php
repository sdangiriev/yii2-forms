<?php

namespace sdangiriev\yii2\forms\themes\bootstrap3;

use sdangiriev\yii2\forms\buttons\AbstractButton;
use sdangiriev\yii2\forms\themes\AbstractTheme;
use sdangiriev\yii2\forms\types\AbstractType;

class Theme extends AbstractTheme
{
    /**
     * {@inheritdoc}
     */
    public $rules = [
        AbstractType::class => FieldCustomizer::class,
        AbstractButton::class => ButtonCustomizer::class,
    ];
}