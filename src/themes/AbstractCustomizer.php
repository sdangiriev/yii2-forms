<?php

namespace sdangiriev\yii2\forms\themes;

use yii\base\BaseObject;
use yii\helpers\Html;
use sdangiriev\yii2\forms\FormElementInterface;

abstract class AbstractCustomizer extends BaseObject implements CustomizerInterface
{
    /**
     * Список классов, которые не должны быть обработаны
     * @var array
     */
    public $exclude = [];

    /**
     * {@inheritdoc}
     */
    public function customize(FormElementInterface $element)
    {
        if ($this->canCustomize($element)) {
            $this->customizeElement($element);
        }
    }

    /**
     * Возвращает значение, определяющее, может ли указанный элемент кастомизирован текущим кастомайзером
     * @param FormElementInterface $element
     * @return bool
     */
    protected function canCustomize(FormElementInterface $element): bool
    {
        $customizable = !isset($element->customizable) || $element->customizable;
        return $customizable && !in_array(get_class($element), $this->exclude);
    }

    /**
     * Реализует логику настройки внешнего вида элемента.
     * В данной реализации будет запускаться только тогда, когда данный элемент должен быть кастомизирован
     * (т.е. учитывается свойство `self::$exclude`).
     * 
     * @param FormElementInterface $element
     */
    abstract protected function customizeElement(FormElementInterface $element);

    /**
     * Добавляет CSS-класс в атрибуты элемента
     * @param array &$options Массив HTML-атрибутов элемента
     * @param string|array $class CSS-класс(ы) которые будут добавлены элементу
     */
    protected function addCssClass(array &$options, $class)
    {
        Html::addCssClass($options, $class);
    }
}
