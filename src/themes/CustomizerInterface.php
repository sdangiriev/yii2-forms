<?php

namespace sdangiriev\yii2\forms\themes;

use sdangiriev\yii2\forms\FormElementInterface;

interface CustomizerInterface
{
    /**
     * Настривает внешний вид элемента
     * @param FormElementInterface $element
     */
    public function customize(FormElementInterface $element);
}
