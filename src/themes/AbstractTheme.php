<?php

namespace sdangiriev\yii2\forms\themes;

use Yii;
use sdangiriev\yii2\forms\FormElementInterface;
use sdangiriev\yii2\forms\CompositeElementInterface;

/**
 * @property array $rules
 */
abstract class AbstractTheme implements ThemeInterface
{
    /**
     * @var array
     */
    protected $_rules = [];

    /**
     * {@inheritdoc}
     */
    public function customize(FormElementInterface $element)
    {
        if (isset($element->customizable) && !$element->customizable) {
            return;
        }
        if ($customizer = $this->getCustomizer($element)) {
            $customizer->customize($element);
        }

        if ($element instanceof CompositeElementInterface) {
            array_map([$this, 'customize'], $element->getElements());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRules(): array
    {
        return $this->_rules;
    }

    /**
     * Добавляет правила обработки элементов. Позволяет добавлять правила через конструктор
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        foreach ($rules as $elementClass => $customizer) {
            $this->addRule($elementClass, $customizer);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addRule($elementClass, $customizerClass)
    {
        $this->_rules[$elementClass] = $customizerClass;
    }

    /**
     * Возвращает кастомайзер для элемента. `null` если он не найден.
     * @param FormElementInterface $element
     * @return CustomizerInterface|null
     */
    protected function getCustomizer(FormElementInterface  $element)
    {
        $config = null;

        foreach ($this->rules as $elementClass => $currentConfig) {
            if ($element instanceof $elementClass) {
                $config = $currentConfig;
            }
        }

        return $config ? $this->createCustomizer($config) : null;
    }

    /**
     * Создает экземпляр `CustomizerInterface`
     * @param array|string $config @see Yii::createObject()
     * @return CustomizerInterface
     */
    protected function createCustomizer($config): CustomizerInterface
    {
        return Yii::createObject($config);
    }
}
