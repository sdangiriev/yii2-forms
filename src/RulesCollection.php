<?php

namespace sdangiriev\yii2\forms;

use ArrayAccess;

/**
 * Класс предназначеный для взаимодействия со списком правил валидации.
 * Позволяет искать правила по имени атрибута и/или имени валидатора.
 */
class RulesCollection implements ArrayAccess
{
    /**
     * Список правил валидации
     * @var array
     * @see \yii\base\Model::rules() для получения данных о формате правил валидации
     */
    private $rules = [];

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * Возвращает список всех правил валидации
     * @return array
     */
    public function all(): array
    {
        return $this->rules;
    }

    /**
     * Возвращает список правил, которые подходят по критерии.
     * Если переданы оба аргумента, вернет только те правила, которые соответствуют им обоим.
     * 
     * @param string $attribute Атрибут, для которого нужно найти правила
     * @param string $validator Имя валидатора, для которого нужно найти правила
     * @return array Список правил которые подошли под критерии
     */
    public function find($attribute, $validator = null): array
    {
        return array_filter($this->all(), function($rule) use ($attribute, $validator) {
            $isAppropriate = true;

            if ($attribute !== null) {
                $isAppropriate &= in_array($attribute, (array) $rule[0]);
            }

            if ($validator !== null) {
                $isAppropriate &= $rule[1] === $validator;
            }

            return $isAppropriate;
        });
    }
    
    /**
     * Добавляет новое правило валидации
     * @param array $rule
     */
    public function addRule(array $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Добавляет массив новых правил валидации
     * @param array $rules
     */
    public function addRules(array $rules)
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }
    }

    /**
     * Проверяет наличие правила валидации
     * @param string $attribute
     * @param string $validator
     * @return bool
     * @see find()
     */
    public function has($attribute, $validator = null): bool
    {
        return (bool) $this->find($attribute, $validator);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->rules[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return isset($this->rules[$offset]) ? $this->rules[$offset] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if ($offset === null) {
            $this->rules[] = $value;
        } else {
            $this->rules[$offset] = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->rules[$offset]);
    }
}
