<?php

namespace sdangiriev\yii2\forms;

/**
 * Интерфейс `FormElementInterface` определяет базовое поведение каждого элемента формы. Будь то поле,
 * кнопка или какой-либо другой тип элемента.
 *
 * @property string $name
 * @property CompositeElementInterface|null $parent
 */
interface FormElementInterface
{
	/**
	 * Возвращает имя элемента
	 * @return string
	 */
	public function getName(): string;

	/**
	 * Устанавливает имя для элемента
	 * @param string $name
	 */
	public function setName(string $name);

	/**
	 * Возвращает полное имя элемента с учетом всех родительских элементов.
	 * Полное имя состоит из имен всех элементов, которые стоят выше по иерархии, и имени текущего элемента
	 * разделенных точкой. Например: 'form.fields.0.type', 'form.fields.0.label' и т.д.
	 * @return string
	 */
	public function resolveName(): string;

	/**
	 * Проверяет наличие родительского элемента
	 * @return bool
	 */
	public function hasParent(): bool;

	/**
	 * Возвращает родительский элемент
	 * @return CompositeElementInterface|null `null`
	 */
	public function getParent(): ?CompositeElementInterface;

	/**
	 * Устанавливает переданный объект как родительский.
	 * Можно передать `null` для того, что бы отвязать элемент от родительского.
	 * @param CompositeElementInterface|null $parent
	 */
	public function setParent(?CompositeElementInterface $parent);

	/**
	 * Определяет видимость элемента
	 * Элемент не должен быть отображен если результат выполения функции === `false`
	 * @return bool
	 */
	public function isVisible(): bool;

	/**
	 * Рендеринг элемента. Метод не должен выводить никакие символы. Результат рендеринга должен возвращаться в виде строки.
	 * @return string
	 */
	public function render(): string;
}
