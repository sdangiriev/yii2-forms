<?php

namespace sdangiriev\yii2\forms;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Model;

/**
 * Класс `AbstractCompositeElement` реализует базовый функционал `CompositeElementInterface`.
 *
 * @property-read array $errors
 * @property-read string $firstError
 */
abstract class AbstractCompositeElement extends AbstractFormField implements CompositeElementInterface
{
    use WrapperOwnerTrait;

    /**
     * Настройка заголовка. Разрешенные элементы:
     * - `tag` - имя тега элемента
     * - `options` - html-атрибуты элемента
     * 
     * @var array
     */
    public $labelOptions = [];

    /**
     * Настройка описание. Разрешенные элементы:
     * - `tag` - имя тега элемента
     * - `options` - html-атрибуты элемента
     * 
     * @var array
     */
    public $descriptionOptions = [];

    /**
     * Определяет, будет ли шаблон элементов наследоваться от родительского если не указан собственный шаблон.
     * @var bool
     * @see getElementsTemplate()
     */
    public $inheritElementsTemplate = true;

    /**
     * Шаблон элемента. Определяет расположение частей текущего элемента.
     * Список доступных частей
     *     - `label` - Заголовок формы (@see renderLabel())
     *     - `description` - Описание формы (@see renderDescription())
     *     - `body` - Тело формы. Содержит все элементы из `self::getElements()` (@see renderBody())
     * 
     * @var string
     */
    protected $_template = "{label}\n{description}\n{body}";

    /**
     * @var array
     */
    protected $_elements = [];

    /**
     * @var Model
     */
    protected $_model;

    /**
     * @var ElementsFactory
     */
    protected $_factory;

    /**
     * @var string|null
     * @see getElementsTemplate()
     */
    protected $_elementsTemplate;

    /**
     * @param string $name
     * @param ElementsFactory $factory Фабрика для создания элементов
     * @param array $config
     */
    public function __construct($name, ElementsFactory $factory, $config = [])
    {
        $this->_factory = $factory;

        /**
         * TODO избавиться от этого костыля
         * 
         * Перемещаем элементы 'elements' & 'data' в конец конфигурационного массива.
         * Порядок элементов в конфигурации важен для инициализации элементов формы и заполнения формы данными.
         *
         * - 'elements' - устанавливается предпоследним, т.к. перед инициализацией элементов формы необходимо
         * что бы все свойства формы были проинициализированны.
         * - 'data' - устанавливается последним, т.к. данные должны загружаться только тогда, когда все элементы
         * формы проинициализированны. В противном случае данные могут быть утеряны т.к. им некуда будет загружаться.
         */
        if (isset($config['elements'])) {
            $config['elements'] = ArrayHelper::remove($config, 'elements', []);
        }
        if (isset($config['data'])) {
            $config['data'] = ArrayHelper::remove($config, 'data', []);
        }

        // Элементы `CompositeElement` по умолчанию не имеют оборачивающего тега
        $config['wrapper'] = $config['wrapper'] ?? false;

        parent::__construct($name, $config);
    }

    /**
     * Возвращает шаблон для дочерних элементов
     * @return string|null
     */
    public function getElementsTemplate(): ?string
    {
        $template = null;
        if ($this->_elementsTemplate !== null) {
            $template = $this->_elementsTemplate;
        } elseif ($this->inheritElementsTemplate && $this->hasParent()) {
            $template = $this->getParent()->getElementsTemplate();
        }

        return $template;
    }

    /**
     * @param string $template
     */
    public function setElementsTemplate(string $template)
    {
        $this->_elementsTemplate = $template;
    }

    /**
     * Возвращает HTML-код заголовка элемента
     * @return string
     */
    protected function renderLabel(): string
    {
        return $this->getLabel()
            ? Html::tag($this->labelOptions['tag'] ?? 'h2', $this->getLabel(), $this->labelOptions['options'] ?? [])
            : '';
    }

    /**
     * Возвращает HTML-код описания элемента
     * @return string
     */
    protected function renderDescription(): string
    {
        return $this->description
            ? Html::tag($this->descriptionOptions['tag'] ?? 'p', $this->description, $this->descriptionOptions['options'] ?? [])
            : '';
    }

    /**
     * Рендеринга тела элемента, т.е. всех дочерних элементов.
     * @return string
     */
    protected function renderBody(): string
    {
        $output = '';
        foreach ($this->getElements() as $element) {
            $output .= $element->render();
        }
        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function has($name): bool
    {
        if (substr_count($name, '.')) {
            $root = $this->getRootCompositeElement($name);
            return $root === null ? false : $root->has($this->excludeRootPart($name));
        }

        return isset($this->_elements[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name): ?FormElementInterface
    {
        if (substr_count($name, '.')) {
            $root = $this->getRootCompositeElement($name);
            return $root === null ? null : $root->get($this->excludeRootPart($name));
        }

        return $this->_elements[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     *
     * - Стандартное добавление элемента в форму
     * `$form->add('username', ['type' => 'string', label' => 'Имя пользователя']);`
     *
     * - Добавление строчного элемента
     * `$form->add('hrDelimeter', '<hr>');`
     *
     * Пример создания формы с вложенными полями
     * ```
     * $form
     *     ->add('username', [
     *          'type' => 'string',
     *          'label' => 'Имя пользователя',
     *      ])
     *      ->add('hrTag', '<hr>')
     *      ->add('profile', [
     *          'type' => 'composite',
     *          'elements' => [
     *              'age' => [
     *                  'type' => 'number',
     *                  'label' => 'Возраст',
     *              ],
     *              // ...
     *          ],
     *      ]);
     * // ИЛИ
     * $form->setElements([
     *     'username' => [
     *         'type' => 'string',
     *         'label' => 'Имя пользователя',
     *     ],
     *     '</hr>',
     *     'profile' => [
     *         'type' => 'composite',
     *         'elements' => [
     *             'age' => [
     *                 'type' => 'number',
     *                 'label' => 'Возраст',
     *             ],
     *             // ...
     *         ],
     *     ],
     * ]);
     * ```
     */
    public function add($name, $config = []): CompositeElementInterface
    {
        $element = $this->createElement($name, $config);
        $this->_elements[$element->getName()] = $element;
        return $this;
    }

    /**
     * {@inheritdoc}
     * При удалении элемента, так же будет сброшен его родиельский элемент
     */
    public function remove($name): CompositeElementInterface
    {
        if (substr_count($name, '.')) {
            $root = $this->getRootCompositeElement($name);
            return $root === null ? $this : $root->remove($this->excludeRootPart($name));
        }

        $element = $this->_elements[$name];
        unset($this->_elements[$name]);
        $element->setParent(null);

        return $this;
    }

    /**
     * Вставляет массив новых элементов в начало формы
     * @param array $elements Массив новых элементов
     * @return self
     */
    public function prepend($elements)
    {
        $this->_elements = array_merge($this->createElements($elements), $this->_elements);
        return $this;
    }

    /**
     * Вставляет массив новых элементов перед указанным элементом.
     * @param string $name Имя элемента, перед которым нужно вставить новые элементы
     * @param array $elements Массив новых элементов
     * @return self
     */
    public function prependBefore($name, $elements)
    {
        if (substr_count($name, '.')) {
            $root = $this->getRootCompositeElement($name);
            return $root === null
                ? $this
                : $root->prependBefore($this->excludeRootPart($name), $elements);
        }

        if ($this->has($name)) {

            $elements = $this->createElements($elements);
            $newElements = [];
            $inserted = false;

            foreach ($this->getElements() as $index => $element) {
                if (!$inserted && $name === $element->getName()) {
                    $newElements = array_merge($newElements, $elements);
                    $inserted = true;
                }
                $newElements[$index] = $element;
            }

            $this->_elements = $newElements;
        }

        return $this;
    }

    /**
     * Добавляет массив новых элементов в конец формы
     * @param array $elements Массив новых элементов
     * @return self
     */
    public function append($elements)
    {
        $this->_elements = array_merge($this->_elements, $this->createElements($elements));
        return $this;
    }

    /**
     * Вставляет массив новых элементов после указанного элемента
     * @param string $name Имя элемента, после которого нужно вставить новые элементы
     * @param array $elements Массив новых элементов
     * @return self
     */
    public function appendAfter($name, $elements)
    {
        if (substr_count($name, '.')) {
            $root = $this->getRootCompositeElement($name);
            return $root === null
                ? $this
                : $root->appendAfter($this->excludeRootPart($name), $elements);
        }

        if ($this->has($name)) {

            $elements = $this->createElements($elements);
            $newElements = [];
            $inserted = false;

            foreach ($this->getElements() as $index => $element) {
                $newElements[$index] = $element;
                if (!$inserted && $name === $element->getName()) {
                    $newElements = array_merge($newElements, $elements);
                    $inserted = true;
                }
            }

            $this->_elements = $newElements;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getElements(): iterable
    {
        return $this->_elements;
    }

    /**
     * {@inheritdoc}
     */
    public function setElements(iterable $elements)
    {
        foreach ($elements as $name => $element) {
            $this->add($name, $element);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasModel(): bool
    {
        return (bool) $this->getModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getModel(): ?Model
    {
        return $this->_model;
    }

    /**
     * {@inheritdoc}
     */
    public function setModel(?Model $model)
    {
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($fieldNames = null): bool
    {
        $valid = true;
        $attributes = [];

        foreach ($this->getElements() as $element) {
            $validationAllowed = $fieldNames === null || in_array($element->getName(), $fieldNames);
            if (!$validationAllowed || !$element instanceof FieldElementInterface) {
                continue;
            }

            if ($element instanceof CompositeElementInterface) {
                $valid &= $element->validate();
            } elseif ($element->isMapped()) {
                $attributes[] = $element->getName();
            }
        }

        if ($attributes) {
            $valid &= $this->getModel()->validate($attributes, false);
        }

        return $valid;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return ArrayHelper::map($this->getFields(), 'name', 'data');
    }

    /**
     * {@inheritdoc}
     */
    public function setData($data)
    {
        if (!is_array($data)) {
            return;
        }

        foreach ($this->getFields() as $field) {
            $fieldData = $data[$field->getName()] ?? ($field instanceof CompositeElementInterface ? [] : null);
            $field->load($fieldData, '');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function load($data, $scope = null): bool
    {
        $scope = $scope ?? $this->getName();
        $data = $scope === '' ? $data : ($data[$scope] ?? []);
        $loaded = false;

        if (is_array($data) && $data) {
            $this->setData($data);
            $loaded = true;
        }

        return $loaded;
    }

    /**
     * Проверяет наличие сообщений об ошибках
     * @param string $name Имя поля, у которого необходимо проверить наличие ошибок
     * @return bool
     */
    public function hasErrors(string $name = null): bool
    {
        $has = false;
        $fields = $name === null
            ? $this->getFields()
            : ($this->has($name) ? [$this->get($name)] : []);

       foreach ($fields as $field) {
            if ($field->hasErrors()) {
                $has = true;
                break;
            }
       }

       return $has;
    }

    /**
     * Возвращает массив сообщений об ошибках поля
     * @param string $name Имя поля, ошибки которого необходимо получить
     * @return array
     */
    public function getErrors(string $name = null): array
    {
        $errors = [];

        if ($name === null) {
            foreach ($this->getFields() as $field) {
                if ($field->hasErrors()) {
                    $errors[$field->getName()] = $field->getErrors();
                }
            }
        } else {
            $errors = $this->has($name) ? $this->get($name)->getErrors() : [];
        }

        return $errors;
    }

    /**
     * Возвращает текст первого особщения об ошибке
     * @param string $name Имя поля, ошибку которого необходимо получить
     * @return string
     */
    public function getFirstError(string $name): string
    {
        $errors = $this->getErrors($name);
        return $errors ? reset($errors) : '';
    }

    /**
     * Добавляет новое сообщение об ошибке
     * @param string $name Имя поля
     * @param string $message Текст ошибки
     */
    public function addError(string $name, string $message)
    {
        $this->get($name)->addError($message);
    }

    /**
     * Массовое добавление сообщений об ошибках
     * @param array $items Массив, ключами которого являются имено полей, а значения - string|string[]
     */
    public function addErrors(array $items)
    {
        foreach ($items as $name => $errors) {
            if ($field = $this->get($name)) {
                is_array($errors) ? $field->addErrors($errors) : $field->addError($errors);
            }
        }
    }

    /**
     * Удаляет все имеющиеся сообщения об ошибках
     * @param string $name Имя поля, ошибки которого необходимо удалить
     */
    public function clearErrors(string $name = null)
    {
        if ($name) {
            if ($this->has($name)) {
                $this->get($name)->clearErrors();
            }
        } else {
            foreach ($this->getFields() as $name => $field) {
                $field->clearErrors();
            }
        }
    }

    /**
     * Возвращает первый элемент из имени, реализующий интерфейс `CompositeElementInterface`.
     * Например, в имени 'fields.0.type' вернет элемент 'fields'.
     * 
     * @param string $name
     * @return CompositeElementInterface|null
     */
    protected function getRootCompositeElement($name): ?CompositeElementInterface
    {
        $rootName = substr($name, 0, strpos($name, '.'));
        $root = $this->_elements[$rootName] ?? null;
        return $root instanceof CompositeElementInterface ? $root : null;
    }

    /**
     * Возвращает имя без первого элемента.
     * Например, если `$name` 'fields.0.type', вернет '0.type'.
     * 
     * @param string $name
     * @return string
     */
    protected function excludeRootPart(string $name): string
    {
        return substr($name, strpos($name, '.') + 1);
    }

    /**
     * Создает элемент и указывает в качестве родителя - себя
     * @param string $name
     * @param array $config
     * @return FormElementInterface
     */
    protected function createElement($name, $config = []): FormElementInterface
    {
        if ($name instanceof FormElementInterface) {
            $element = $name;
            $element->setParent($this);
        } elseif ($config instanceof FormElementInterface) {
            $element = $config;
            $element->setParent($this);
        } elseif (is_array($config)) {
            $config['parent'] = $this;
        }

        $element = $this->getFactory()->create($name, $config);

        if (!$element->hasParent()) {
            $element->setParent($this);
        }

        return $element;
    }

    /**
     * Создает массив элементов из массива конфигураций.
     * Для создания элементов использует метод `createElement()`.
     * 
     * @param array[] $configs Массив конфигураци элементов, ключи которого - это имена элементов, а значения - конфигурация
     * @return array
     */
    protected function createElements($configs): array
    {
        $elements = [];
        foreach ($configs as $name => $config) {
            $elements[$name] = $this->createElement($name, $config);
        }
        return $elements;
    }

    /**
     * Возвращает экземпляр `ElementsFactory`, необходимый для создания элементов внутри данного класса
     * @return ElementsFactory
     */
    protected function getFactory(): ElementsFactory
    {
        return $this->_factory;
    }

    /**
     * @return FieldElementInterface[]
     */
    protected function getFields(): array
    {
        return array_filter($this->getElements(), function($element) {
            return $element instanceof FieldElementInterface;
        });
    }
}
