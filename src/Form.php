<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\helpers\Html;

class Form extends CompositeElement
{
    public const MESSAGE_STATUS_INFO = 'info';
    public const MESSAGE_STATUS_SUCCESS = 'success';
    public const MESSAGE_STATUS_ERROR = 'error';

    /**
     * @var string
     */
    public $action = '';

    /**
     * @var string
     */
    public $method = 'post';

    /**
     * @var string
     */
    public $enctype = '';

    /**
     * HTML-атрибуты формы
     * @var array
     */
    public $options = [];

    /**
     * Настройки отображения сообщений формы.
     * @var array
     */
    public $messageOptions = [];

    /**
     * Соотношения имен статусов сообщений и их CSS-классов
     * Структура:
     *     - key - Имя статуса
     *     - value - CSS-класс который будет добавлен сообщению с соответствующим статусом
     * Пример:
     * ```php
     * public $messageCssClasses = [
     *     self::MESSAGE_STATUS_INFO => 'alert-info',
     *     self::MESSAGE_STATUS_SUCCESS => 'alert-success',
     *     self::MESSAGE_STATUS_ERROR => 'alert-danger',
     * ];
     * ```
     * 
     * @var null|array Если `null`, то элементу сообщения в ролие CSS-класса будет добавлено имя статуса.
     */
    public $messageCssClasses;

    /**
     * Сообщения формы
     * @var array
     */
    private $_messages = [];

    /**
     * {@inheritdoc}
     */
    protected $_template = "{label}\n{message}\n{description}\n{body}";

    /**
     * {@inheritdoc}
     *
     * Рендерит форму. Сначала будут рендериться содержимое формы, потом это все будет оборачиваться в тег `<form>`.
     * Такой порядок позволяет в процессе рендеринга изменять настройки формы.
     * Пример использования: Один из полей с типом 'file' в момент рендеринга может автоматически изменить 'form[enctype]'
     * формы на 'multipart/form-data' что бы позволить загружать файлы в формы,
     * что так же повышает удобство, т.к. это избавляет от необходимости постоянно 'form[enctype]' вручную.
     */
    public function render(): string
    {
        if (!$this->isVisible()) {
            return '';
        }

        $body = parent::render();

        return $this->renderBegin() . $body . $this->renderEnd();
    }

    /**
     * Рендерит первое сообщение формы из `self::$_messages`
     * @return string
     */
    protected function renderMessage(): string
    {
        $output = '';

        if ($messages = $this->getMessages()) {
            $firstMessage = reset($messages);
            $messageOptions = $this->messageOptions ?? [];
            $messageOptions['options'] = $messageOptions['options'] ?? [];

            $messageStatusClass = $this->messageCssClasses === null
                ? $firstMessage['status']
                : ($this->messageCssClasses[$firstMessage['status']] ?? $firstMessage['status']);

            Html::addCssClass($messageOptions['options'], $messageStatusClass);

            $template = $messageOptions['template'] ?? '{message}';

            $output = $this->wrap(
                strtr($template, ['{message}' => $firstMessage['text']]),
                $messageOptions
            );
        }

        return $output;
    }

    /**
     * Рендерит открывающий тег формы.
     * Если форма является частью другой формы (т.е. имеет родителя),
     * то открывающий и закрывающие теги формы сгенерированы не будут.
     * @return string
     */
    public function renderBegin(): string
    {
        if ($this->hasParent()) {
            return '';
        }

        if ($this->enctype) {
            $this->options['enctype'] = $this->enctype;
        }

        return Html::beginForm($this->action, $this->method, $this->options);
    }

    /**
     * Рендерит закрывающий тег формы
     * @return string
     */
    public function renderEnd(): string
    {
        return $this->hasParent() ? '' : Html::endForm();
    }

    /**
     * Добавляет информационное сообщение для формы.
     * @param string $message
     * @param string $status
     */
    public function addMessage(string $message, string $status = self::MESSAGE_STATUS_INFO): self
    {
        $this->_messages[] = [
            'text' => $message,
            'status' => $status,
        ];

        return $this;
    }

    /**
     * Возвращает список всех сообщений формы.
     * Каждый элемент списка имеет следущую структуру:
     * ```php
     * [
     *     'text' => (string),
     *     'status' => (string),
     * ]
     * ```
     * Где,
     *     - `text` - текст сообщения,
     *     - `status` - статус сообщений (@see MESSAGE_STATUS_* константы)
     * 
     * @return array
     */
    public function getMessages(): array
    {
        return $this->_messages;
    }
}
