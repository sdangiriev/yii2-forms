<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class FormsFactory
{
    /**
     * Список доступных элементов формы и реализующие их классы
     * @var array key - алиас, value - имя класса элемента
     */
    private static $classesMap = [
        // Поля
        'number' => \sdangiriev\yii2\forms\types\NumberType::class,
        'string' => \sdangiriev\yii2\forms\types\StringType::class,
        'text' => \sdangiriev\yii2\forms\types\TextType::class,
        'dropdown' => \sdangiriev\yii2\forms\types\DropDownListType::class,
        'checkbox' => \sdangiriev\yii2\forms\types\CheckboxType::class,
        'file' => \sdangiriev\yii2\forms\types\FileType::class,
        'date' => \sdangiriev\yii2\forms\types\DateType::class,
        'time' => \sdangiriev\yii2\forms\types\TimeType::class,
        'hidden' => \sdangiriev\yii2\forms\types\HiddenType::class,
        'widget' => \sdangiriev\yii2\forms\types\WidgetType::class,
        'relation' => \sdangiriev\yii2\forms\types\RelationType::class,

        // Кнопки
        'button' => \sdangiriev\yii2\forms\buttons\InputButton::class,
        'submitButton' => \sdangiriev\yii2\forms\buttons\SubmitButton::class,
        'resetButton' => \sdangiriev\yii2\forms\buttons\ResetButton::class,

        // Особые
        'raw' => \sdangiriev\yii2\forms\RawElement::class,
        'composite' => \sdangiriev\yii2\forms\CompositeElement::class,
        'collection' => \sdangiriev\yii2\forms\CollectionElement::class,
    ];

    public static function create(string $class, array $config = []): FormElementInterface
    {
        return Yii::$container->get($class, [], $config);
    }

    /**
     * Создает экземпляр формы с указанноый конфигурацией.
     * @param array $config Конфигурация формы
     * @return Form
     */
    public static function createForm($config = []): Form
    {
        return Yii::$container->get(Form::class, [], $config);
    }

    public static function createFormByTemplate($templateConfig, $formConfig = [])
    {
        $form = static::createForm($formConfig);
        $template = static::createTemplate($templateConfig);
        $template->buildForm($form);
        return $form;
    }

    public static function createTemplate(string $class): FormTemplateInterface
    {
        if (!is_subclass_of($class, FormTemplateInterface::class)) {
            throw new InvalidConfigException(sprintf('Form template class must implement `%s`.', FormTemplateInterface::class));
        }

        return Yii::$container->get($class);
    }

    /**
     * Создает экземпляр `FormElementInterface`
     * @param  array $config
     * @return FormElementInterface
     */
    public static function createFormElement($name, $config): FormElementInterface
    {
        $type = ArrayHelper::remove($config, 'type');

        if (!$type) {
            throw new InvalidConfigException('The type of the form element must be set.');
        } elseif (isset(static::$classesMap[$type])) {
            $class = static::$classesMap[$type];
        } elseif (class_exists($type)) {
            $class = $type;
        } else {
            throw new InvalidConfigException("Unknown field type: {$type}");
        }

        return Yii::$container->get($class, [$name], $config);
    }
}