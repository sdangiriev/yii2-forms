<?php

namespace sdangiriev\yii2\forms;

/**
 * Интерфейс `FieldElementInterface` представляет элемент поля в форме, например 'input[type=checkbox]', 'input[type=text]' и т.д.
 * Элементы, реализующий этот интерфейс имею возможность хранения данных и их загрузки в при метода `load()`.
 * В то время как `FormElementInterface` реализует базовое поведение абсолютно каждого элемента формы,
 * `FieldElementInterface` реализует поведение, которое свойственно полям формы, т.е. элементам формы, которые могут
 * состоять из различных частей (например заголовок, описание, текст ошибки и т.д.), хранить в себе какие-либо данные
 * и иметь заголовок / описание для пользователей.
 * Особенности поведения данного интерфейса:
 * - Хранение и загрузка данных в элемент (@see getData(), setData(), load())
 * - Получение информации об элементе (@see getLabel() / setLabel(), getDescription() / setDescription())
 * - Возможность указывать расположение частей элемента при рендеринге (@see getTemplate(), setTemplate())
 *
 * @property mixed $data
 * @property string $label
 * @property string $description
 * @property string $template
 */
interface FieldElementInterface extends FormElementInterface
{
    /**
     * Возвращает данные элемента
     * @return mixed Тип данных у каждого конкретного элемента может отличаться
     */
    public function getData();

    /**
     * Устанавливает переданные данные элементу
     * @param mixed $data Тип данных у каждого конкретного элемента может отличаться
     */
    public function setData($data);

    /**
     * Загрузка данных в элемента
     * @param mixed $data
     * @param string|null $scope
     * @return bool Результат загрузки данных
     */
    public function load($data, $scope = ''): bool;

    /**
     * Возвращает заголовк элемента
     * @return string
     */
    public function getLabel(): string;

    /**
     * Устанавливает заголовок элемента
     * @param string $label
     */
    public function setLabel($label);

    /**
     * Возвращает описание элемента
     * @return string
     */
    public function getDescription(): string;

    /**
     * Устанавливает описание элемента
     * @param string $description
     */
    public function setDescription($description);

    /**
     * Возвращает шаблон расположения частей элемента.
     * Каждый элемент может состоять из различных частей, например заголовок, описание, текст ошибки и т.д.
     * Все эти части могу рендериться разными способами. Данный шаблон описывает расположение этих частей.
     * 
     * @return string
     */
    public function getTemplate();

    /**
     * Устанавливает шаблон элемента
     * @param string $template
     */
    public function setTemplate($template);
}
