;(function($) {
    var Collection = (function(settings) {
        /**
         * CSS-селектор коллекции (должен быть уникальным)
         * @type {String}
         */
        this.collectionSelector = settings.collectionSelector;
        /**
         * CSS-селектор кнопки добавления нового элемента
         * @type {String}
         */
        this.addElementButtonSelector = settings.addElementButtonSelector;
        /**
         * CSS-селектор кнопки удаления элемента
         * @type {String}
         */
        this.deleteElementButtonSelector = settings.deleteElementButtonSelector;
        /**
         * CSS-селектор обертки элемента коллекции
         * @type {String}
         */
        this.collectionElementSelector = settings.collectionElementSelector;
        /**
         * Имя класса прототипа элемента коллекции
         * @type {String}
         */
        this.prototypeElementCssClass = settings.prototypeElementCssClass;
        /**
         * Cтрока, все вхождения в код прототипа которой будут замененены на индекс элемента при добавлении нового элемента в коллекцию
         * @type {String}
         */
        this.prototypeIndexKey = settings.prototypeIndexKey === undefined ? '{index}' : settings.prototypeIndexKey;

        var canAdd = settings.canAdd === undefined ? undefined : Boolean(settings.canAdd);
        var canDelete = settings.canDelete === undefined ? undefined : Boolean(settings.canDelete);

        var self = this;
        var $prototype;
        var counter = $(this.collectionElementSelector, this.collectionSelector).not('.' + this.prototypeElementCssClass).length;

        /**
         * Добавляет новый элемент в коллекцию.
         * Элемент будет добавлен если в коллекции присутствовал прототип элемента.
         */
        this.addElement = function() {
            if (canAddElements()) {
                this
                    .createNewElement()
                    .appendTo(this.collectionSelector);
            }
        };

        /**
         * Удаляет элемент коллекции
         * @param {jQuery} removeButton Кнопка удаления элемента
         */
        this.deleteElement = function(removeButton) {
            if (canDeleteElements) {
                $(removeButton)
                    .closest(self.collectionElementSelector)
                    .remove();
            }
        };

        /**
         * Создает новый HTML элемент коллекции
         * @return {jQuery}
         */
        this.createNewElement = function() {
            var $html = $prototype.get(0).outerHTML;
            return $($html.replace(new RegExp(this.prototypeIndexKey, 'g'), this.getNextIndex()));
        }

        /**
         * Возвращает следующий индекс элемента коллекции.
         * Индекс начинается с нуля.
         * Атрибут "data-index" это внутренний атрибут коллекции, который служит для идентификации элементов.
         * @return {Integer}
         */
        this.getNextIndex = function() {
            while($('[data-index="' + counter + '"]', this.collectionSelector).length) {
                counter++;
            }
            // Возвращает индекс которого еще нет в коллекции и сразу же инкрементирует его
            // Это нужно что бы в следующий раз при получении индекса не заходить в цикл
            return counter++;
        }

        /**
         * Функция проверяет возможность добавления новых элементов в коллекцию.
         * Если была заполнена переменная `canAdd`, вернет ее значение.
         * Иначе, вернет `true` только в случае если прототип элемента был инициализирован
         * и селектор кнопки для добавления новых элементов (свойство `this.addElementButtonSelector`) указан.
         * @return {Boolean}
         */
        var canAddElements = function() {
            return canAdd === undefined ? $prototype.length && self.addElementButtonSelector.length : canAdd;
        }

        /**
         * Определяет, могут ли удаляться элементы коллекции.
         * Если заполнена переменная `canDelete`, вернет ее значение.
         * Иначе, вернет `true` только в случае если заполнен селектор (свойство `this.deleteElementButtonSelector`) кнопки удаления элемента.
         * @return {Boolean}
         */
        var canDeleteElements = function() {
            return canDelete === undefined ? self.deleteElementButtonSelector.length : canDelete;
        }

        /**
         * Инициализация коллекции. Если коллекция для элемента уже была проинициализирована,
         * повторная инициализация запущена не будет.
         * Добавляет экземпляр класса Collection в data HTML элемента коллекции.
         * Инициализирует прототип.
         * Добавляет обработчики событий.
         */
        var init = function() {
            if ($(self.collectionSelector).data('form.collection')) {
                return false;
            }
            $(self.collectionSelector).data('form.collection', self);
            initPrototype();
            initEventHandlers();
        }

        var initPrototype = function() {
            if (self.prototypeElementCssClass) {
                $prototype = $('.' + self.prototypeElementCssClass, self.collectionSelector);
                $prototype.removeClass(self.prototypeElementCssClass);
                $prototype.remove();
            }
        };

        var initEventHandlers = function() {
            if (canAddElements()) {
                $(self.addElementButtonSelector).on('click', function(event) {
                    self.addElement();
                });
            }
            if (canDeleteElements()) {
                $(self.collectionSelector).delegate(self.deleteElementButtonSelector, 'click', function() {
                    self.deleteElement(this);
                });
            }
        };

        init();
    });

    window.Collection = Collection;
})(jQuery);