<?php

namespace sdangiriev\yii2\forms;

abstract class AbstractFormField extends AbstractElement implements FieldElementInterface
{
    /**
     * @var string
     * @see getLabel(), setLabel()
     */
    protected $_label = '';

    /**
     * @var string
     * @see getDescription(), setDescription()
     */
    protected $_description = '';

    /**
     * @var string
     * @see getTemplate(), setTemplate()
     */
    protected $_template = '';

    /**
     * {@inheritdoc}
     */
    public function getLabel(): string
    {
        return $this->_label;
    }

    /**
     * {@inheritdoc}
     */
    public function setLabel($label)
    {
        $this->_label = $label;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return $this->_description;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     * {@inheritdoc}
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
    }

    /**
     * Рендеринг элемента.
     * Рендеринг происходит по шаблону, который указан в `self::$template`.
     * Каждый элемент состоит из частей, например заголовок, описание, текст ошибки и т.д.
     * Каждая часть элемента рендерится отдельным методом. Сигнатура метода следующая:
     * 
     * ```php
     * protected function render{partName}(): string
     * {
     *     // Тут происходит вся магия ...
     *     return $output;
     * }
     * ```
     * 
     * где, {partName} - имя части элемента.
     *
     * Данный метод перед рендерингом ищет объявленные в шаблоне части элемента
     * (это вставки подобные '{partName}', т.е. имя части, заключенное в фигурные скобки).
     * Затем, для каждой найденой части имет метод, который сможет ее отрендерить по следующему шаблону: 'render{partName}',
     * где подстрока '{partName}' будет заменена на имя части элемента.
     * Например, для рендеринга заголовка в шаблоне необходимо будет указать такую подстроку - '{label}'
     * и создать метод 'renderLabel()' который возвращает HTML-код части элемента.
     * 
     * @return string
     */
    public function render(): string
    {
        if (!$this->isVisible()) {
            return '';
        }

        // before render event

        $output = $this->getTemplate();

        foreach ($this->parseTemplate($output) as $part) {
            $method = 'render' . ucfirst($part);
            $replaceTo = method_exists($this, $method) ? $this->$method() : '';
            $output = strtr($output, ['{' . $part . '}' => $replaceTo]);
        }

        // after render event

        return $this->wrap($output);
    }

    /**
     * Возвращает имена частей, вставки которых были найдены в переданном шаблоне.
     * Например, для шаблона "<a href='...'>{title}</a>{description}" вернет следующий массив:
     * ```php
     * ['title', 'description'];
     * ```
     * 
     * @param string $template
     * @return string[]
     */
    protected function parseTemplate(string $template): array
    {
        return preg_match_all('/\{([\w]+)\}/', $template, $matches) ? $matches[1] : [];
    }
}