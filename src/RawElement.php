<?php

namespace sdangiriev\yii2\forms;

class RawElement extends AbstractElement
{
	/**
	 * @var string
	 */
	public $body = '';

	/**
	 * {@inheritdoc}
	 */
	public function render(): string
	{
		return $this->body;
	}
}
