<?php

namespace sdangiriev\yii2\forms;

use yii\base\Model;

/**
 * @property array $elements
 * @property Model|null $model
 */
interface CompositeElementInterface extends FormElementInterface
{
    /**
     * Проверяет наличие элемента в форме
     * @param string $id
     * @return bool
     */
    public function has($id): bool;

    /**
     * Возвращает элемент формы с указанным ID
     * Обращение через точку пока не поддерживается
     * @param string $id
     * @return FormElementInterface|null
     */
    public function get($id): ?FormElementInterface;

    /**
     * Добавляет новый элемент в форму
     * @param mixed $id ID элемента в форме
     * @param array $config Конфигурация для создания элемента
     * @return self
     */
    public function add($id, $value = null): self;

    /**
     * Удаляет элемент из формы
     * @param stirng $id
     * @return self
     */
    public function remove($id): self;

    /**
     * Возвращает список всех элементов формы
     * @return iterable
     */
    public function getElements(): iterable;

    /**
     * Устанавливает новые элементы в форму
     * @param iterable $elements
     */
    public function setElements(iterable $elements);

    /**
     * @return bool
     */
    public function hasModel(): bool;

    /**
     * @return Model|null
     */
    public function getModel(): ?Model;

    /**
     * @param Model|null $model
     */
    public function setModel(?Model $model);

    /**
     * Валидация данных элемента
     * @param array $fieldNames
     * @return bool
     */
    public function validate($fieldNames = []): bool;
}
