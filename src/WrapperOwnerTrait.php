<?php

namespace sdangiriev\yii2\forms;

use yii\helpers\Html;

/**
 * Добавляет `protected` метод в класс, который позволяет легко оборачивать элементы в указанный html-тэг
 */
trait WrapperOwnerTrait
{
    /**
     * Каждое поле содержит родительский элемент в который оно будет помещено
     * Данное свойство содержит набор атрибутов для родительского элемента
     * 
     * @var array|bool
     * `bool` - `false` - оборачивающий тег не будет сгенерирован, `true` - оборачивающий тег будет сгенерирован с настрйоками по умолчанию
     * `array` - оборачивающий тег будет сгенерирован с указанными настройками
     *     Доступные свойства:
     *     - `tag` - имя тега. По умолчанию 'div'.
     *     - `options` - HTML-атрибуты элемента.
     *     
     */
    public $wrapper = [];

    /**
     * Оборачивает строку `$content` в тег, указанный в `self::$wpapper['tag']`
     * @param string $content
     * @return string
     * @see $wrapper
     */
    protected function wrap(string $content, $options = null): string
    {
        $wrapper = $options === null ? $this->wrapper : $options;
        return $wrapper === false
            ? $content
            : Html::tag($wrapper['tag'] ?? 'div', $content, $wrapper['options'] ?? []);
    }
}