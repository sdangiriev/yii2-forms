<?php

namespace sdangiriev\yii2\forms;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use sdangiriev\yii2\forms\assets\FormCollectionsAsset;

/**
 * Класс, который представляет коллекцию элементов формы.
 * Например, пользователь может иметь неопределенно количество email-адресов или телефонов.
 * 
 * ```php
 * $collection = new CollectionElement([
 *     'elementTemplate' => [
 *         'type' => 'email',
 *     ],
 * ]);
 * ```
 *
 * При генерации формы будет сгенерированы поля для ввода email-адресов. Так же коллекции позволяют добавлять удалять
 * элементы.
 *
 * Количество элементов коллекции управляется при помощи загруженных в элемент данных (@see CollectionElement::setData()).
 * При загрузке данных сверяются индексы загруженных и существующих элементов. Если загружаются данные для элементов
 * которых нет в коллекции, новые элементы будут созданы. Если для каких то элементов данные загружены не были, эти 
 * элементы будут удалены.
 *
 * Класс `CollectionElement` позволяет генерировать прототип элемента коллекции для упрощения процесса добавления
 * новых элементов на клиентской стороне.
 */
class CollectionElement extends CompositeElement
{
    /**
     * Шаблон элемента коллекции
     * @var array|callable
     * @see setData()
     */
    public $elementTemplate = [];

    /**
     * Определяет, будет ли отображаться шаблон элемента коллекции.
     * Шаблон необходим в случаях, когда коллекция поддерживает возможность добавления новых элементов
     * @var bool
     */
    public $renderPrototype = false;

    /**
     * Ключ для замены в прототипе.
     * При генерации прототипа элемента коллекции, вместо индекса будет вставлено значение этого свойство.
     * При использовании на клиентской стороне при помощи JavaScript можно будет заменить вхождение данной строки
     * на настоящий индекс элемента.
     * Этот ключ будет вставляться в имя input элемента, в html-атрибут ID, html-классы и т.д.
     * @var string
     */
    public $prototypeIndexKey = '{index}';

    /**
     * Регулирует подключение клиентской логики для управления колекцией
     * Если значение равно `false`, клиентский скрипт не будет зарегистрирован.
     * @var bool
     */
    public $registerClientScripts = true;

    /**
     * Регулирует возможность добавления новых элементов в коллекцию
     * TODO Подумать над тем, что бы с помощью этого свойство управлять добавление новых элементов в методе `self::setData()`
     * @var bool
     */
    public $canAddElements = true;

    /**
     * Регулирует удаление элементов коллекции
     * TODO Подумать над тем, что бы с помощью этого свойство управлять удалением элементов в методе `self::setData()`
     * @var bool
     */
    public $canDeleteElements = true;

    /**
     * CSS-класс для коллекции
     * @var string
     */
    public $collectionCssClass = 'collection';

    /**
     * CSS-класс для элемента коллекции
     * @var string
     */
    public $collectionElementCssClass = 'collection__element';

    /**
     * CSS-класс для прототипа элемента коллекции
     * @var string
     */
    public $prototypeElementCssClass = 'collection__prototype';

    /**
     * CSS-селектор кнопки, при нажатии на которую должно проиходить добавление элемента
     * @var string
     */
    public $addElementButtonSelector = '';

    /**
     * CSS-селектор кнопки внутри элемента коллекции, при нажатии на которую должно происходить удаление элемента
     * @var string
     */
    public $deleteElementButtonSelector = '';

    /**
     * Счетчик, необходимый для генерации идентификаторов коллекций
     * @var integer
     */
    private static $counter = 0;

    /**
     * Префикс для идентификатора коллекции
     * @var string
     */
    private $idPrefix = 'cltn';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerClientScripts();
    }

    /**
     * {@inheritdoc}
     *
     * Рендеринг коллекции.
     * Перед началом рендеринга, если параметр `self::$renderPrototype` == `true`, первым в списке элементов коллекции
     * будет добавлен прототип элемента.
     * Сразу после рендеринга он будет удален из списка.
     */
    public function render(): string
    {
        if ($this->canAddElements && $this->renderPrototype) {
            $prototype = $this->createPrototype();
            if ($theme = $this->getTheme()) {
                $theme->customize($prototype);
            }
            array_unshift($this->_elements, $prototype);
        }

        $this->wrapper['options'] = $this->wrapper['options'] ?? [];
        Html::addCssClass($this->wrapper['options'], $this->collectionCssClass);

        if (!isset($this->wrapper['options']['id']) || !$this->wrapper['options']['id']) {
            $this->wrapper['options']['id'] = $this->generateUniqueId();
        }

        $result = parent::render();

        if ($this->canAddElements && $this->renderPrototype) {
            array_shift($this->_elements);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     * 
     * Заполняет элемент данными
     * Если были загружены новые данные для элементов которых до этого не было в коллекции - новые элементы для этих данных будут созданы
     * Если для какого-то элемента данные загружены не были - он будет удален
     * Таким образом поддерживается актуальность элементов коллекции
     */
    public function setData($data)
    {
        $loaded = false;
        $elements = [];
        $currentElementsList = $this->getElements();

        foreach ($data as $index => $value) {
            $element = $currentElementsList[$index] ?? $this->createElement($index);
            $loaded |= $element->load($value, '');
            $elements[$index] = $element;
        }

        $this->_elements = $elements;

        return $loaded;
    }

    /**
     * {@inheritdoc}
     * 
     * Не делает ничего, т.к. элементы в коллекции создаются динамически
     */
    public function setElements(iterable $elements)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function add($id, $value = null): CompositeElementInterface
    {
        return $this;
    }

    /**
     * Регистрация web-ресурсов (js, css, etc) и клиентской логики для управления коллекцией.
     * Регистрация JS зависит от следующих свойств:
     * - `self::$registerClientScripts` - должно быть `true`
     *     - Будет зарегистрирован скрипт для взаимодействия с коллекцией через JavaScript
     * - Рендеринг прототипа включен и разрешено либо добавлять, либо удалять элементы коллекции
     *     - Будет инициализирован скрипт для управления данной коллекцией
     */
    private function registerClientScripts()
    {
        if ($this->getView() && $this->registerClientScripts) {

            FormCollectionsAsset::register($this->getView());

            if ($this->renderPrototype && $this->canAddElements || $this->canDeleteElements) {
                $clientOptions = [
                    'collectionSelector' => ".{$this->collectionCssClass}",
                    'collectionElementSelector' => ".{$this->collectionElementCssClass}",
                    'addElementButtonSelector' => $this->addElementButtonSelector,
                    'deleteElementButtonSelector' => $this->deleteElementButtonSelector,
                    'prototypeElementCssClass' => $this->prototypeElementCssClass,
                    'prototypeIndexKey' => $this->prototypeIndexKey,
                    'canAdd' => $this->canAddElements,
                    'canDelete' => $this->canDeleteElements,
                ];
                $this->getView()->registerJs('new Collection(' . json_encode($clientOptions) . ');');
            }
        }
    }

    /**
     * Создает прототип элемента коллекции
     * Прототип необходим для использования на клиентсокй стороне для добавления новых элементов коллекции.
     * @return FormElementInterface
     */
    private function createPrototype(): FormElementInterface
    {
        $config = $this->getTemplateConfig();
        $config['wrapper']['options'] = $config['wrapper']['options'] ?? [];
        Html::addCssClass($config['wrapper']['options'], $this->prototypeElementCssClass);
        return $this->createElement($this->prototypeIndexKey, $config);
    }

    /**
     * Создает экземпляр элемента коллекции. Для создания используется шаблон конфигурации,
     * указанный в `self::elementTemplate`
     * @param string $name
     * @param array $config Конфигурация элемента. Если не передана, будет использована конфигурация из свойства `self::$elementTemplate`
     * @return FormElementInterface
     */
    protected function createElement($name, $config = null): FormElementInterface
    {
        $config = $config === null ? $this->getTemplateConfig() : $config;

        $config['wrapper']['options'] = $config['wrapper']['options'] ?? [];
        Html::addCssClass($config['wrapper']['options'], $this->collectionElementCssClass);
        $config['wrapper']['options']['data']['index'] = $name;

        return parent::createElement($name, $config);
    }

    /**
     * Возвращает уникальный идентификатор коллекции
     * @return string
     */
    private function generateUniqueId(): string
    {
        return $this->idPrefix . self::$counter++;
    }

    /**
     * Возвращает конфигурационный массив для создания нового элемента коллекции.
     * @return array
     */
    private function getTemplateConfig(): array
    {
        return is_array($this->elementTemplate) ? $this->elementTemplate : $this->elementTemplate();
    }
}
