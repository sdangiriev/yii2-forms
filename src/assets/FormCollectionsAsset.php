<?php

namespace sdangiriev\yii2\forms\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class FormCollectionsAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $sourcePath = '@sdangiriev/yii2/forms/web';

    /**
     * {@inheritdoc}
     */
    public $css = [
        'css/forms.css',
    ];

    /**
     * {@inheritdoc}
     */
    public $js = [
        'js/forms.js'
    ];

    /**
     * {@inheritdoc}
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
