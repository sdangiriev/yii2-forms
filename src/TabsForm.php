<?php

namespace sdangiriev\yii2\forms;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * !Внимание! Данный класс генерирует базовую структуру для реализации формы с вкладками.
 * Для получения окончательного вида формы с вкладками используйте темы для кастомизации данного элемента.
 * 
 * Класс `TabForm` дает возможность создать форму с вкладками.
 * Отличие данного класса от других элементов формы заключается в том, что рендеринг разделен на 2 этапа.
 * 1 этап это рендеринг элементов вкладок, т.е. заголовкок, с которыми будет взаимодействовать пользователь.
 * 2 этап - рендеринг контейнеров с содержимым вкладок.
 * 
 * Пример создания такой формы:
 * ```php
 * $form = $factory->create('tabform', [
 *      'activeTab' => 'secondTab',
 *      'elements' => [
 *          'firstTab' => [
 *              'type' => 'composite',
 *              'elements' => [...],
 *          ],
 *          'secondTab' => [
 *              'type' => 'composite',
 *              'elements' => [...],
 *          ],
 *          'thirdTab' => [
 *              'type' => 'composite',
 *              'elements' => [...],
 *          ],
 *      ],
 * ]);
 * ```
 *
 * В результате рендеринга получается примерно следующая структура:
 * ```html
 * <div class="tab-form">
 *      <ul class="tab-form__navs">
 *          <li>Title for first tab</li>
 *          <li>Title for second tab</li>
 *          <li>Title for third tab</li>
 *      </ul>
 *      <div class="tab-form__contents">
 *          <div>Content of first tab</div>
 *          <div class="active">Content of second tab</div>
 *          <div>Content of third tab</div>
 *      </div>
 *  </div>
 * ```
 */
class TabsForm extends Form
{
    private static $counter = 0;

    /**
     * Настройки контейнера с вкладками.
     * Данный контейнер содержит заголовки вкладок.
     * 
     * @var array
     */
    public $headerContainerOptions = [];

    /**
     * Настройки контейнера с содержимым вкладок
     * @var array
     */
    public $bodyContainerOptions = [];

    /**
     * Имя текущего активного таба
     * @var string
     */
    public $activeTab;

    /**
     * CSS-класс для активного таба. Будет добавлен заголовку активной вкладки.
     * @var string
     */
    public $activeTabCssClass = 'active';

    /**
     * CSS-класс для контейнера с содержимым активного таба.
     * @var string
     */
    public $activeBodySectionCssClass = 'active';

    /**
     * Список опций для настройки клиентской логики
     * @var array
     *  ```php
     *  $clientOptions = [
     *      'containerSelector' => (string),
     *      'headerContainerSelector' => (string),
     *      'headerItemSelector' => (string),
     *      'bodyContainerSelector' => (string),
     *      'bodyItemSelector' => (string),
     *  ];
     *  ```
     */
    public $clientOptions = [];

    private $idPrefix = 'tabs';

    /**
     * {@inheritdoc}
     */
    protected $_template = "{label}\n{description}\n{header}\n{body}";

    /**
     * {@inheritdoc}
     * При инициализации проверяет что бы все дочерние элементы реализовывали интерфейс `CompositeElementInterface`
     * @throws InvalidConfigException Если дочерние элементы не наследуют интферфейс CompositeElementInterface
     */
    public function init()
    {
        parent::init();

        foreach ($this->getElements() as $element) {
            /** @var FormElementInterface $element */
            if (!$element instanceof CompositeElementInterface) {
                throw new InvalidConfigException(strtr('Element with name "{name}" in TabsForm must implement `{interface}`.', [
                    '{name}' => $element->getName(),
                    '{interface}' => CompositeElementInterface::class,
                ]));
            }
        }

        $this->wrapper = $this->wrapper ?: [];
        $this->wrapper['options']['id'] = $this->generateUniqueId();

        $this->settingActiveTab();
    }

    /**
     * Рендеринг заголовков вкладок
     * @return string
     */
    protected function renderHeader(): string
    {
        $output = '';

        foreach ($this->getElements() as $tab) {
            /** @var CompositeElement $tab */
            if ($this->activeTab && $this->activeTabCssClass && $tab->getName() === $this->activeTab) {
                $tab->labelOptions['options'] = $tab->labelOptions['options'] ?? [];
                Html::addCssClass($tab->labelOptions['options'], $this->activeTabCssClass);
            }
            $output .= $tab->renderLabel();
        }

        $wrapperOptions = ArrayHelper::merge([
            'tag' => 'ul',
        ], $this->headerContainerOptions ?? []);

        return $this->wrap($output, $wrapperOptions);
    }

    /**
     * Рендеринг контейнеров с содержимым вкладок
     * @return string
     */
    protected function renderBody(): string
    {
        $output = '';

        foreach ($this->getElements() as $tab) {
            if ($this->activeTab && $this->activeBodySectionCssClass && $tab->getName() === $this->activeTab) {
                $tab->wrapper['options'] = $tab->wrapper['options'] ?? [];
                Html::addCssClass($tab->wrapper['options'], $this->activeTabCssClass);
            }
            $output .= $tab->wrap($tab->renderDescription() . $tab->renderBody());
        }

        return $this->wrap($output, $this->bodyContainerOptions ?? []);
    }

    /**
     * Определяет активную вкладку. Если активная вкладка не определена, активной будет установлена первая в списке.
     */
    private function settingActiveTab()
    {
        if ($this->activeTab !== null) {
            return;
        }

        $elements = $this->getElements();
        $firstTab = reset($elements);
        $this->activeTab = $firstTab->getName();
    }

    private function generateUniqueId(): string
    {
        return $this->idPrefix . self::$counter++;
    }
}